-- CREATE DATABASE "ASVEL_3CSI" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8';
-- \connect "ASVEL_3CSI"

--
-- Data for Name: typecertificat; Type: TABLE DATA; Schema: public; Owner: postgres
-- 

INSERT INTO typecertificat (idtypecertificat, nomtypecertificat) VALUES (1, 'Loisir');
INSERT INTO typecertificat (idtypecertificat, nomtypecertificat) VALUES (2, 'Compétition');

--
-- Data for Name: federation; Type: TABLE DATA; Schema: public; Owner: postgres
-- 

INSERT INTO federation (idfederation, nomfederation, nbjoursvaliditeaffiliation) VALUES (1, 'FFME', -1);
INSERT INTO federation (idfederation, nomfederation, nbjoursvaliditeaffiliation) VALUES (2, 'FFS', -2);
INSERT INTO federation (idfederation, nomfederation, nbjoursvaliditeaffiliation) VALUES (3, 'Licence découverte journée', 1);
INSERT INTO federation (idfederation, nomfederation, nbjoursvaliditeaffiliation) VALUES (4, 'Licence découverte WE', 2);
INSERT INTO federation (idfederation, nomfederation, nbjoursvaliditeaffiliation) VALUES (5, 'Carte membre', -1);
INSERT INTO federation (idfederation, nomfederation, nbjoursvaliditeaffiliation) VALUES (6, 'Invité', 2);
INSERT INTO federation (idfederation, nomfederation, nbjoursvaliditeaffiliation) VALUES (7, 'Membre club partenaire', -2);

--
-- Data for Name: typelicense; Type: TABLE DATA; Schema: public; Owner: postgres
-- 

INSERT INTO typelicense (idlicense, nomtypelicense) VALUES (1, 'Loisir');
INSERT INTO typelicense (idlicense, nomtypelicense) VALUES (2, 'Compétition');
INSERT INTO typelicense (idlicense, nomtypelicense) VALUES (3, 'Licence Adulte');
INSERT INTO typelicense (idlicense, nomtypelicense) VALUES (4, 'Licence Jeune');
INSERT INTO typelicense (idlicense, nomtypelicense) VALUES (5, 'Licence Famille');

--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
-- 

insert into role (idrole, nomrole) VALUES (1, 'Administrateur');
insert into role (idrole, nomrole) VALUES (2, 'Secrétaire');
insert into role (idrole, nomrole) VALUES (3, 'Encadrant');
insert into role (idrole, nomrole) VALUES (4, 'Comptable');
insert into role (idrole, nomrole) VALUES (5, 'Adhérent');

--
-- Data for Name: adherent; Type: TABLE DATA; Schema: public; Owner: postgres
-- 

-- Administrateur de test (id = -1, mail = admin@asvel.net, mdp = 123)
INSERT INTO adherent (idadherent, nomadherent, prenomadherent, datenaissanceadherent, adresseadherent, mailadherent, teladherent, cpadherent, villeadherent, paysadherent, nationaliteadherent, mdpadherent, commentaireadherent, datederniereinscriptionadherent) VALUES (-1, 'Administrateur', 'deBase', '1901-01-01', 'Adresse de l"admin', 'admin@asvel.net', '0102030405', '69100', 'Villeurbanne', 'France', 'Français', '$2y$10$7GJeuSkDTSxlzJvIP2P6c.gsz1rcfVM3F3qQ93DtrWf6U5pzAjeb2', 'Administrateur pour la configuration de l''application', '1901-01-01');

-- Données de test (mdp = 123)
INSERT INTO adherent (idadherent, nomadherent, prenomadherent, datenaissanceadherent, adresseadherent, mailadherent, teladherent, cpadherent, villeadherent, paysadherent, nationaliteadherent, mdpadherent, commentaireadherent, datederniereinscriptionadherent) VALUES (-2, 'Secrétaire', 'deTest', '1978-08-10', 'Adresse de la secrétaire', 'secretaire@asvel.net', '0102030405', '69100', 'VILLEURBANNE', 'France', 'Française', '$2y$10$7GJeuSkDTSxlzJvIP2P6c.gsz1rcfVM3F3qQ93DtrWf6U5pzAjeb2', 'Secrétaire', '1901-01-01');
INSERT INTO adherent (idadherent, nomadherent, prenomadherent, datenaissanceadherent, adresseadherent, mailadherent, teladherent, cpadherent, villeadherent, paysadherent, nationaliteadherent, mdpadherent, commentaireadherent, datederniereinscriptionadherent) VALUES (-3, 'Encadrant', 'deTest', '1981-05-12', 'Adresse de l"encadrant', 'encadrant@asvel.net', '0102030405', '69007', 'LYON', 'France', 'Français', '$2y$10$7GJeuSkDTSxlzJvIP2P6c.gsz1rcfVM3F3qQ93DtrWf6U5pzAjeb2', 'Encadrant', '1901-01-01');
INSERT INTO adherent (idadherent, nomadherent, prenomadherent, datenaissanceadherent, adresseadherent, mailadherent, teladherent, cpadherent, villeadherent, paysadherent, nationaliteadherent, mdpadherent, commentaireadherent, datederniereinscriptionadherent) VALUES (-4, 'Comptable', 'deTest', '1972-01-24', 'Adresse du comptable', 'comptable@asvel.net', '0102030405', '69002', 'LYON', 'France', 'Français', '$2y$10$7GJeuSkDTSxlzJvIP2P6c.gsz1rcfVM3F3qQ93DtrWf6U5pzAjeb2', 'Comptable', '1901-01-01');
INSERT INTO adherent (idadherent, nomadherent, prenomadherent, datenaissanceadherent, adresseadherent, mailadherent, teladherent, cpadherent, villeadherent, paysadherent, nationaliteadherent, mdpadherent, commentaireadherent, datederniereinscriptionadherent) VALUES (-5, 'Adhérent', 'deTest', '1970-07-21', 'Adresse de l"adhérent', 'adherent@asvel.net', '0102030405', '69120', 'Retonfey', 'France', 'Français', '$2y$10$7GJeuSkDTSxlzJvIP2P6c.gsz1rcfVM3F3qQ93DtrWf6U5pzAjeb2', 'Adhérent', '1901-01-01');

--
-- Data for Name: etre; Type: TABLE DATA; Schema: public; Owner: postgres
-- 

INSERT INTO etre (idadherent, idrole) VALUES (-1, 1);
INSERT INTO etre (idadherent, idrole) VALUES (-2, 2);
INSERT INTO etre (idadherent, idrole) VALUES (-3, 3);
INSERT INTO etre (idadherent, idrole) VALUES (-4, 4);
--INSERT INTO etre (idadherent, idrole) VALUES (-5, 5);

--
-- Data for Name: affilier; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO affilier (idadherent, idfederation, idtypelicense, idtypereglementadherent, montantreglementadherent, dateaffiliation, numerolicense, idaffiliation, datefinaffiliation) VALUES (-1, 1, 3, NULL, NULL, '2019-09-27', '873', 1, NULL);
INSERT INTO affilier (idadherent, idfederation, idtypelicense, idtypereglementadherent, montantreglementadherent, dateaffiliation, numerolicense, idaffiliation, datefinaffiliation) VALUES (-2, 1, 3, NULL, NULL, '2019-11-07', '304', 3, NULL);
INSERT INTO affilier (idadherent, idfederation, idtypelicense, idtypereglementadherent, montantreglementadherent, dateaffiliation, numerolicense, idaffiliation, datefinaffiliation) VALUES (-3, 1, 4, NULL, NULL, '2019-10-03', '941', 10, NULL);
INSERT INTO affilier (idadherent, idfederation, idtypelicense, idtypereglementadherent, montantreglementadherent, dateaffiliation, numerolicense, idaffiliation, datefinaffiliation) VALUES (-4, 1, 4, NULL, NULL, '2019-09-01', '15', 11, NULL);
INSERT INTO affilier (idadherent, idfederation, idtypelicense, idtypereglementadherent, montantreglementadherent, dateaffiliation, numerolicense, idaffiliation, datefinaffiliation) VALUES (-5, 1, 3, NULL, NULL, '2019-09-01', '877', 13, NULL);

--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tag (idtag, nomtag) VALUES (1, 'SAE');
INSERT INTO tag (idtag, nomtag) VALUES (2, 'SAE jeune');
INSERT INTO tag (idtag, nomtag) VALUES (3, 'Ski alpin');
INSERT INTO tag (idtag, nomtag) VALUES (4, 'Alpi');
INSERT INTO tag (idtag, nomtag) VALUES (5, 'Ski rando');

--
-- Data for Name: avoir; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO avoir (idadherent, idtag) VALUES (-1, 1);
INSERT INTO avoir (idadherent, idtag) VALUES (-2, 2);
INSERT INTO avoir (idadherent, idtag) VALUES (-3, 3);
INSERT INTO avoir (idadherent, idtag) VALUES (-4, 4);
INSERT INTO avoir (idadherent, idtag) VALUES (-5, 5);

--
-- Data for Name: difficulte; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO difficulte (iddifficulte, nomdifficulte) VALUES (1, '*');
INSERT INTO difficulte (iddifficulte, nomdifficulte) VALUES (2, '**');
INSERT INTO difficulte (iddifficulte, nomdifficulte) VALUES (3, '***');
INSERT INTO difficulte (iddifficulte, nomdifficulte) VALUES (4, '****');
INSERT INTO difficulte (iddifficulte, nomdifficulte) VALUES (5, 'F');
INSERT INTO difficulte (iddifficulte, nomdifficulte) VALUES (6, 'PD');
INSERT INTO difficulte (iddifficulte, nomdifficulte) VALUES (7, 'AD');
INSERT INTO difficulte (iddifficulte, nomdifficulte) VALUES (8, 'D');
INSERT INTO difficulte (iddifficulte, nomdifficulte) VALUES (9, 'Initiation');
INSERT INTO difficulte (iddifficulte, nomdifficulte) VALUES (10, 'Tous niveaux');


--
-- Data for Name: raisonannulation; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (1, 'Météo');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (2, 'Encadrant indisponible');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (3, 'Manque de participants');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (4, 'Pas de voiture');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (5, 'COVID');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (6, 'Travail');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (7, 'Blessé');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (8, 'Malade');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (9, 'Raison personnelle');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (10, 'Pas de place');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (11, 'Participation rejetée par l&#39;encadrant');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (12, 'Fusion de sortie');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (13, 'Indisponible suite au décalage de sortie');
INSERT INTO raisonannulation (idannulation, libelleannulation) VALUES (14, 'Erreur de sa part lors de l&#39;inscription en ligne');


--
-- Data for Name: sortie; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sortie (idsortie, nomsortie, lieusortie, datedebutsortie, datefinsortie, distancesortie, denivellesortie, altitudemaxsortie, heuredepart, commentairessortie, tarifsortie, coutautoroute, nombreparticipantsmaxsortie, nbvoituressortie, iddifficulte, idorganisateur, idraisonannulation, inscriptionenligne, fraisannexe, fraisannexedetails) VALUES (1, '2020A1', 'à définir', '2020-05-16', '2020-05-17', NULL, NULL, NULL, '07:00', NULL, NULL, NULL, NULL, NULL, 5, -1, 5, false, NULL, NULL);
INSERT INTO sortie (idsortie, nomsortie, lieusortie, datedebutsortie, datefinsortie, distancesortie, denivellesortie, altitudemaxsortie, heuredepart, commentairessortie, tarifsortie, coutautoroute, nombreparticipantsmaxsortie, nbvoituressortie, iddifficulte, idorganisateur, idraisonannulation, inscriptionenligne, fraisannexe, fraisannexedetails) VALUES (2, '2020R1', 'Monts du Lyonnais', '2020-05-21', '2020-05-21', NULL, NULL, NULL, '08:00', NULL, NULL, NULL, NULL, NULL, 1, -2, 5, false, NULL, NULL);
INSERT INTO sortie (idsortie, nomsortie, lieusortie, datedebutsortie, datefinsortie, distancesortie, denivellesortie, altitudemaxsortie, heuredepart, commentairessortie, tarifsortie, coutautoroute, nombreparticipantsmaxsortie, nbvoituressortie, iddifficulte, idorganisateur, idraisonannulation, inscriptionenligne, fraisannexe, fraisannexedetails) VALUES (3, '2020 R6', 'Mont Aiguille (date à reconfirmer)', '2020-06-21', '2020-06-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, -3, 2, false, NULL, NULL);
INSERT INTO sortie (idsortie, nomsortie, lieusortie, datedebutsortie, datefinsortie, distancesortie, denivellesortie, altitudemaxsortie, heuredepart, commentairessortie, tarifsortie, coutautoroute, nombreparticipantsmaxsortie, nbvoituressortie, iddifficulte, idorganisateur, idraisonannulation, inscriptionenligne, fraisannexe, fraisannexedetails) VALUES (4, '2020 R36 ', 'Pilat', '2020-06-14', '2020-06-14', 108, 900, 1420, '07:30', 'ajout d&#39;une sortie déconfinement', 11, 2, 8, 2, 1, -4, NULL, false, NULL, NULL);
INSERT INTO sortie (idsortie, nomsortie, lieusortie, datedebutsortie, datefinsortie, distancesortie, denivellesortie, altitudemaxsortie, heuredepart, commentairessortie, tarifsortie, coutautoroute, nombreparticipantsmaxsortie, nbvoituressortie, iddifficulte, idorganisateur, idraisonannulation, inscriptionenligne, fraisannexe, fraisannexedetails) VALUES (5, '2020 R13', 'Le Grand Galbert', '2020-07-26', '2020-07-26', 292, 1100, NULL, NULL, 'Fabrice encadrant !', NULL, 23.4, 10, 2, 2, -5, NULL, false, NULL, NULL);
INSERT INTO sortie (idsortie, nomsortie, lieusortie, datedebutsortie, datefinsortie, distancesortie, denivellesortie, altitudemaxsortie, heuredepart, commentairessortie, tarifsortie, coutautoroute, nombreparticipantsmaxsortie, nbvoituressortie, iddifficulte, idorganisateur, idraisonannulation, inscriptionenligne, fraisannexe, fraisannexedetails) VALUES (6, '2020 R12', 'La Pyramide Taillefer (à confirmer)', '2020-07-19', '2020-07-19', 314, NULL, NULL, '06:30', NULL, 3, 23.4, 8, 1, 3, -1, NULL, false, NULL, NULL);
INSERT INTO sortie (idsortie, nomsortie, lieusortie, datedebutsortie, datefinsortie, distancesortie, denivellesortie, altitudemaxsortie, heuredepart, commentairessortie, tarifsortie, coutautoroute, nombreparticipantsmaxsortie, nbvoituressortie, iddifficulte, idorganisateur, idraisonannulation, inscriptionenligne, fraisannexe, fraisannexedetails) VALUES (7, '2020 A9', 'Rocher des Patres (Belledonne)', '2020-10-17', '2020-10-18', 290, NULL, NULL, NULL, NULL, 15, 23.8, 10, 2, 6, -2, NULL, false, NULL, NULL);
INSERT INTO sortie (idsortie, nomsortie, lieusortie, datedebutsortie, datefinsortie, distancesortie, denivellesortie, altitudemaxsortie, heuredepart, commentairessortie, tarifsortie, coutautoroute, nombreparticipantsmaxsortie, nbvoituressortie, iddifficulte, idorganisateur, idraisonannulation, inscriptionenligne, fraisannexe, fraisannexedetails) VALUES (8, 'SR16 2021', 'Crète de Brouffier', '2021-02-06', '2021-02-06', 200, NULL, NULL, '06:45', 'La météo est mauvaise dimanche. Donc sortie annulée dimanche, la sortie est décalée au samedi 6 février. Remboursement que du trajet retour de l&#39;Encadrant.', NULL, 12, 6, 1, 1, -3, NULL, false, NULL, NULL);
INSERT INTO sortie (idsortie, nomsortie, lieusortie, datedebutsortie, datefinsortie, distancesortie, denivellesortie, altitudemaxsortie, heuredepart, commentairessortie, tarifsortie, coutautoroute, nombreparticipantsmaxsortie, nbvoituressortie, iddifficulte, idorganisateur, idraisonannulation, inscriptionenligne, fraisannexe, fraisannexedetails) VALUES (9, 'SR16 2022', 'Crète de Brouffier', '2021-02-06', '2021-02-06', 200, NULL, NULL, '06:45', 'La météo est mauvaise dimanche. Donc sortie annulée dimanche, la sortie est décalée au samedi 6 février. Remboursement que du trajet retour de l&#39;Encadrant.', NULL, 12, 6, 1, 1, -3, NULL, false, NULL, NULL);

--
-- Data for Name: activite; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO activite (idactivite, nomactivite)  VALUES (1, 'Ski de randonnée');
INSERT INTO activite (idactivite, nomactivite)  VALUES (2, 'Ski de Fond');
INSERT INTO activite (idactivite, nomactivite)  VALUES (3, 'Ski Alpin');
INSERT INTO activite (idactivite, nomactivite)  VALUES (4, 'Alpinisme');
INSERT INTO activite (idactivite, nomactivite)  VALUES (5, 'Randonnée');
INSERT INTO activite (idactivite, nomactivite)  VALUES (6, 'Randonnée raquettes');
INSERT INTO activite (idactivite, nomactivite)  VALUES (7, 'Via ferrata');
INSERT INTO activite (idactivite, nomactivite)  VALUES (8, 'Canyoning');
INSERT INTO activite (idactivite, nomactivite)  VALUES (9, 'Escalade');

--
-- Data for Name: concerner; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO concerner (idsortie, idactivite) VALUES (1, 1);
INSERT INTO concerner (idsortie, idactivite) VALUES (2, 2);
INSERT INTO concerner (idsortie, idactivite) VALUES (3, 3);
INSERT INTO concerner (idsortie, idactivite) VALUES (4, 4);
INSERT INTO concerner (idsortie, idactivite) VALUES (5, 5);
INSERT INTO concerner (idsortie, idactivite) VALUES (6, 6);
INSERT INTO concerner (idsortie, idactivite) VALUES (7, 7);
INSERT INTO concerner (idsortie, idactivite) VALUES (8, 8);
INSERT INTO concerner (idsortie, idactivite) VALUES (9, 9);

--
-- Data for Name: mail; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO mail (idmail, datemail, sujetmail, contenumail, idadherentexpediteur, destinatairesvisibles, mailexpediteur) VALUES (1, '2020-06-10 07:05:08', 'sz', 'qs', 67, false, NULL);
INSERT INTO mail (idmail, datemail, sujetmail, contenumail, idadherentexpediteur, destinatairesvisibles, mailexpediteur) VALUES (2, '2020-06-10 07:06:25', 'test ASVEL', 'Test ASVEL', 67, false, NULL);

--
-- Data for Name: fonction; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO fonction (idfonction, nomfonction) VALUES (1, 'Participant');
INSERT INTO fonction (idfonction, nomfonction) VALUES (2, 'Encadrant');
INSERT INTO fonction (idfonction, nomfonction) VALUES (3, 'Co-encadrant');

--
-- Data for Name: typereglement; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO typereglement (idtypereglement, nomtypereglement) VALUES (1, 'Espèces');
INSERT INTO typereglement (idtypereglement, nomtypereglement) VALUES (2, 'Carte Bancaire');
INSERT INTO typereglement (idtypereglement, nomtypereglement) VALUES (3, 'Chèque');
INSERT INTO typereglement (idtypereglement, nomtypereglement) VALUES (4, 'Chèque vacances');

--
-- Data for Name: participer; Type: TABLE DATA; Schema: public; Owner: postgres
--

Delete from participer ;		-- Effacer les participations des encadrants, déjà créées par les triggers
INSERT INTO participer (idadherent, idsortie, dateinscription, montantarrhes, montanttotal, commentaireparticipation, idfonction, idraisonannulation, idtypereglementarrhes, idtypereglementmontanttotal, validationparticipation, nbplacevoiture, dva, pellesonde, idinscripteur, arrhesremboursees, excluducovoiturage, debutant, louemateriel) VALUES (-1, 1, '2020-05-07 11:38:00', 0, 0, '', 2, NULL, NULL, NULL, true, 0, false, false, NULL, NULL, NULL, false, false);
INSERT INTO participer (idadherent, idsortie, dateinscription, montantarrhes, montanttotal, commentaireparticipation, idfonction, idraisonannulation, idtypereglementarrhes, idtypereglementmontanttotal, validationparticipation, nbplacevoiture, dva, pellesonde, idinscripteur, arrhesremboursees, excluducovoiturage, debutant, louemateriel) VALUES (-2, 2, '2020-06-04 18:13:00', 3, 0, '', 1, NULL, 1, NULL, true, 0, false, false, -2, false, NULL, false, false);
INSERT INTO participer (idadherent, idsortie, dateinscription, montantarrhes, montanttotal, commentaireparticipation, idfonction, idraisonannulation, idtypereglementarrhes, idtypereglementmontanttotal, validationparticipation, nbplacevoiture, dva, pellesonde, idinscripteur, arrhesremboursees, excluducovoiturage, debutant, louemateriel) VALUES (-3, 3, '2020-06-04 17:35:00', 3, 0, '', 1, NULL, 1, NULL, true, 0, false, false, -3, false, NULL, false, false);
INSERT INTO participer (idadherent, idsortie, dateinscription, montantarrhes, montanttotal, commentaireparticipation, idfonction, idraisonannulation, idtypereglementarrhes, idtypereglementmontanttotal, validationparticipation, nbplacevoiture, dva, pellesonde, idinscripteur, arrhesremboursees, excluducovoiturage, debutant, louemateriel) VALUES (-4, 4, '2020-07-23 15:19:00', 0, 3, '', 1, NULL, NULL, 1, true, 0, false, false, -4, false, false, false, false);
INSERT INTO participer (idadherent, idsortie, dateinscription, montantarrhes, montanttotal, commentaireparticipation, idfonction, idraisonannulation, idtypereglementarrhes, idtypereglementmontanttotal, validationparticipation, nbplacevoiture, dva, pellesonde, idinscripteur, arrhesremboursees, excluducovoiturage, debutant, louemateriel) VALUES (-5, 5, '2020-06-04 17:43:00', 0, 0, 'A rembourser car attente', 1, 3, NULL, NULL, false, 4, false, false, -5, NULL, NULL, false, false);
INSERT INTO participer (idadherent, idsortie, dateinscription, montantarrhes, montanttotal, commentaireparticipation, idfonction, idraisonannulation, idtypereglementarrhes, idtypereglementmontanttotal, validationparticipation, nbplacevoiture, dva, pellesonde, idinscripteur, arrhesremboursees, excluducovoiturage, debutant, louemateriel) VALUES (-1, 6, '2020-06-04 17:35:00', 3, 0, '', 1, NULL, 1, NULL, true, 0, false, false, -1, false, NULL, false, false);
INSERT INTO participer (idadherent, idsortie, dateinscription, montantarrhes, montanttotal, commentaireparticipation, idfonction, idraisonannulation, idtypereglementarrhes, idtypereglementmontanttotal, validationparticipation, nbplacevoiture, dva, pellesonde, idinscripteur, arrhesremboursees, excluducovoiturage, debutant, louemateriel) VALUES (-2, 7, '2020-06-04 18:12:00', 5, 8, '', 1, NULL, 3, 1, true, 0, false, false, -2, true, NULL, false, false);
INSERT INTO participer (idadherent, idsortie, dateinscription, montantarrhes, montanttotal, commentaireparticipation, idfonction, idraisonannulation, idtypereglementarrhes, idtypereglementmontanttotal, validationparticipation, nbplacevoiture, dva, pellesonde, idinscripteur, arrhesremboursees, excluducovoiturage, debutant, louemateriel) VALUES (-3, 8, '2020-06-04 17:31:00', 20, 15, 'Préfère ne pas prendre sa voiture', 1, NULL, 1, 1, true, 4, false, false, -3, true, false, false, false);