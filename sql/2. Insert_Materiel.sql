insert into TypeMiseAdispo(idTypeMiseAdispo, nomTypeMiseAdispo)
Values 
(-1, 'location'),
(-2, 'vente');


insert into TypeMateriel(idTypeMateriel, nomTypeMateriel)
values 
(-1, 'vetement'),
(-2, 'ski'),
(-3, 'protection'),
(-4, 'appareil photo');

insert into materiel(taillemateriel, pointuremateriel, modelemateriel, latitude1materiel, latitude2materiel, 
longitude1materiel, longitude2materiel, dateachatmateriel, commentairemateriel, payscartemateriel, 
idadherent, idtypemateriel, idtypemiseadispo)
values
(31, 34, 'syK', 43.0, 45.0, 1.3, 2.34, '26/12/2022', 'test', 'France', -1, -1, -1),
(31, 34, 'alpin', 43.0, 5.0, 12.0, 12.4, '26/12/2022', 'test2', 'France', -2, -2, -2),
(31, 34, 'yyyyK', 43.0, 4.0, 10.3, 1.4, '26/12/2022', 'test', 'France', -3, -3, -2);