function redir(){
    //    variable de redirection
    var obj = window.location.replace("info-participant.php?idAdherent=" + $("#idadherent").val() + "&idSortie=" + $("#idsortie").val());
}
function redir_ListeParticipants(){
    //    variable de redirection
    var obj = window.location.replace("liste-participant.php?idSortie=" + $("#idsortie").val());
}

var form = $('#formparticiper');
form.submit(function (event) {
    var data = form.serialize();
    var urlSource = window.location.href;
    var urlDest = urlSource.substring(0, urlSource.lastIndexOf('/') + 1);
    urlDest += "traitement-participant.php";
    $.post(urlDest, data)
            .done(function (data) {
                if (data && data === 'true') {
                    var toasterOptionCloseButton = toastr.options.closeButton;
                    var toasterOptionTimeout = toastr.options.timeOut;
                    toastr.options.closeButton = true;
                    toastr["success"]("Le participant a été enrengistré");
                    toastr.options.timeOut = toasterOptionTimeout;
                    toastr.options.closeButton = toasterOptionCloseButton;
//                    fonction de redirection (temps en millisecondes)
                    setTimeout(redir, 800);
                } else {
                    toastr["error"]("Une erreur est survenue : " + data);
                }
            })
            .fail(function (error) {
                toastr["error"]("Une erreur est survenue : " + error.responseText);
            });
    event.preventDefault();

});

function allerAuParticipant(piIdAdherent, piIdSortie) {
//    alert(piIdParticipant) ;
    if (piIdAdherent === "cboGoToParticipant") {
        // Prendre la valeur choisir dans la combobox
        piIdAdherent = $("#cboGoToParticipant").val() ;
    }
    var obj = window.location.replace("info-participant.php?idAdherent=" + piIdAdherent + "&idSortie=" + piIdSortie);
}

function supprimerParticipant(event, piIdAdherent, piIdSortie) {
    if (!confirm("Souhaitez-vous réellement supprimer cette participation ?")) {
        // alert("Suppression confirlmée") ;
        return ;
    }
    var urlSource = window.location.href;
    var urlDest = urlSource.substring(0, urlSource.lastIndexOf('/') + 1);
    urlDest += "supprimer-participant.php";
    data = {"idAdherent": piIdAdherent, "idSortie": piIdSortie};
    $.post(urlDest, data)
        .done(function (data) {
            if (data && data === 'OK') {
                var toasterOptionCloseButton = toastr.options.closeButton;
                var toasterOptionTimeout = toastr.options.timeOut;
                toastr.options.closeButton = true;
                toastr["success"]("La participation a été effacée");
                toastr.options.timeOut = toasterOptionTimeout;
                toastr.options.closeButton = toasterOptionCloseButton;
                setTimeout(redir_ListeParticipants, 800);
            } else {
                toastr["error"]("Une erreur est survenue : " + data);
            }
        })
        .fail(function (error) {
            toastr["error"]("Une erreur est survenue : " + error.responseText);
        });
    event.preventDefault();
    
}

function voirInfosAdherent() {
    var lsURL = "info-adherent.php?id=" + $("#idadherent").val() ;
    window.open(lsURL) ;
}

function voirSortiesAdherent() {
    var lsURL = "liste-sortie.php?Adhérent=" + $("#idadherent").val() ;
    window.open(lsURL) ;
}

$(document).ready(function () {

    var modalAdherent = $('#modalInfoAdherent');
    var formAdherent = $('#formAdherentModal');
    modalAdherent.on('show.bs.modal', function(event){
       if ($("#idadherent").val()) {
           alert("Cette fonctionnalité n'est disponible que pour une nouvelle participation, pour laquelle on n'a pas encore défini l'adhérent participant.") ;
           modalAdherent.modal("hide") ;
           return event.preventDefault();
       }
    });
    formAdherent.submit(function (event) {
        // Lancer l'enregistrement, en demandant d'invoquer afterSaveAdherent en cas de succès
        enregistrerAdherent(formAdherent, afterSaveAdherent) ;
        event.preventDefault();
    });
    function afterSaveAdherent(psIdAdherentCréé){
        var option = '<option value="' + psIdAdherentCréé + '" selected >' 
                + formAdherent.find("#nomAdherent").val() + " " + formAdherent.find("#prenomAdherent").val()
                + '</option>' ;
        $("#idadherent").append(option) ;
        modalAdherent.modal("hide") ;
       
    }
    
// LG 20200901 début : convertir la combo des adhérents en select2
// Il faut utiliser un timeout, sans quoi ca ne se fait pas bien
    window.setTimeout( function() {
        $('#idadherent').select2({placeholder: "Choisissez un adhérent"});
        $('.select2-container').addClass("form-control") ;
        $('.select2-selection').css("border-style", "none") ;
    }, 100) ;
// LG 20200901 fin

});


