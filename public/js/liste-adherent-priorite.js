function déValiderParticipation(event, piIdAdherent, piIdSortie) {
    return validerParticipation(event, piIdAdherent, piIdSortie, true) ;
}
function validerParticipation(event, piIdAdherent, piIdSortie, pbDévalider) {
    event.preventDefault();
    
    $.post("valider-participation.php", {idSortie: piIdSortie, idAdherent: piIdAdherent, dévalider: pbDévalider?"true":"false"})
        .done(function (data) {
            if (data && data === 'true') {
                var toasterOptionCloseButton = toastr.options.closeButton;
                var toasterOptionTimeout = toastr.options.timeOut;
                var toasterOptionOnHidden = toastr.options.onHidden ;
                toastr.options.closeButton = true;
                toastr.options.onHidden = function() {
// $(event.srcElement).html("a été validé") ;
                    if (pbDévalider) {
                        var lsText = " <a href='' onclick='validerParticipation(event, " + piIdAdherent + ", " + piIdSortie + ")'>valider</a>" ;
                    } else {
                        var lsText = " <a href='' onclick='déValiderParticipation(event, " + piIdAdherent + ", " + piIdSortie + ")'>dévalider</a>" ;                                                        
                    }
                    $(event.srcElement).html(lsText) ;
                };
                toastr.options.preventDuplicates = true ;
                toastr["success"]("OK");
                toastr.options.timeOut = toasterOptionTimeout;
                toastr.options.closeButton = toasterOptionCloseButton;
                toastr.options.onHidden = toasterOptionOnHidden;
            } else {
                toastr["error"]("Une erreur est survenue : " + data);
            }
        })
        .fail(function (error) {
            toastr["error"]("Une erreur est survenue : " + error.responseText);
        });
}

function modifierParticipation(event, piIdAdherent, piIdSortie, pbDévalider) {
    event.preventDefault();
    window.open('info-participant.php?idAdherent=' + piIdAdherent + "&idSortie=" + piIdSortie, '_blank');
}