var form = $('#formadherent');
form.submit(function (event) {
//    var data = form.serialize();
//    var urlSource = window.location.href;
//    var urlDest = urlSource.substring(0, urlSource.lastIndexOf('/') + 1);
//    urlDest += "traitement-adherent.php";
//    $.post(urlDest, data)
//            .done(function (data) {
//                console.log(data)
//                if (data && parseInt(data)) {
//                    // On a reçu l'Id de l'item enregistré
//                    $("#idAdherent").val(parseInt(data));
//                    var toasterOptionCloseButton = toastr.options.closeButton;
//                    var toasterOptionTimeout = toastr.options.timeOut;
//                    toastr.options.closeButton = true;
//                    toastr["success"]("L'adhérent a été enrengistré");
//                    toastr.options.timeOut = toasterOptionTimeout;
//                    toastr.options.closeButton = toasterOptionCloseButton;
//                } else {
//                    toastr["error"]("Une erreur est survenue : " + data);
//                }
//            })
//            .fail(function (error) {
//                toastr["error"]("Une erreur est survenue : " + error.responseText);
//            });
    enregistrerAdherent() ;
    event.preventDefault();
});

function enregistrerAdherent(submittedForm, callback) {
    if (!submittedForm) {
        // On a pas indique quel formulaire a été validé : prendre le formulaire par défaut
        submittedForm = form ;
    }
    var data = submittedForm.serialize();
    var urlSource = window.location.href;
    var urlDest = urlSource.substring(0, urlSource.lastIndexOf('/') + 1);
    urlDest += "traitement-adherent.php";
    $.post(urlDest, data)
            .done(function (data) {
                console.log(data)
                if (data && parseInt(data)) {
                    // On a reçu l'Id de l'item enregistré
                    $("#idAdherent").val(parseInt(data));
                    var toasterOptionCloseButton = toastr.options.closeButton;
                    var toasterOptionTimeout = toastr.options.timeOut;
                    toastr.options.closeButton = true;
                    toastr["success"]("L'adhérent a été enrengistré");
                    toastr.options.timeOut = toasterOptionTimeout;
                    toastr.options.closeButton = toasterOptionCloseButton;
                    if (callback) callback(data) ;
                } else {
                    toastr["error"]("Une erreur est survenue : " + data);
                }
            })
            .fail(function (error) {
                toastr["error"]("Une erreur est survenue : " + error.responseText);
            });
    
}

function supprimerAdherent(event, piIdAdherent, pbMuet, callback) {
    if (!pbMuet && !confirm("Souhaitez-vous réellement supprimer cet adhérent ?")) {
        return;
    }
    var urlSource = window.location.href;
    var urlDest = urlSource.substring(0, urlSource.lastIndexOf('/') + 1);
    urlDest += "supprimer-adherent.php";
    data = {"idAdherent": piIdAdherent};
    $.post(urlDest, data)
        .done(function (data) {
            if (data && data === 'OK') {
                var toasterOptionCloseButton = toastr.options.closeButton;
                var toasterOptionTimeout = toastr.options.timeOut;
                toastr.options.closeButton = true;
                toastr["success"]("L'adherent a été effacé");
                toastr.options.timeOut = toasterOptionTimeout;
                toastr.options.closeButton = toasterOptionCloseButton;
//                setTimeout(redir_ListeParticipants, 800);
                if (callback) {
                    callback(piIdAdherent);
                }
            } else {
                var toasterOptionTimeout = toastr.options.timeOut;  // LG 20220910
                toastr.options.timeOut = 0;                         // LG 20220910
                toastr["error"]("Une erreur est survenue : " + data);
                toastr.options.timeOut = toasterOptionTimeout;      // LG 20220910
            }
        })
        .fail(function (error) {
            var toasterOptionTimeout = toastr.options.timeOut;  // LG 20220910
            toastr.options.timeOut = 0;                         // LG 20220910
            toastr["error"]("Une erreur est survenue : " + error.responseText);
            toastr.options.timeOut = toasterOptionTimeout;      // LG 20220910
        });
    if (event) {
        event.preventDefault();
    }
}

// LG 20220910
function fusionnerAdherents(event, piIdAdherentAConserver, piIdAdherentAVirer, pbMuet, callback) {
    if (!pbMuet && !confirm("Souhaitez-vous réellement fusionner ces adhérents ?")) {
        return;
    }
    var urlSource = window.location.href;
    var urlDest = urlSource.substring(0, urlSource.lastIndexOf('/') + 1);
    urlDest += "fusionner-adherents.php";
    data = {"idAdherentAConserver": piIdAdherentAConserver, "idAdherentAVirer": piIdAdherentAVirer};
    $.post(urlDest, data)
        .done(function (data) {
            if (data && data.substring(0, 2) === 'OK') {
                var toasterOptionCloseButton = toastr.options.closeButton;
                var toasterOptionTimeout = toastr.options.timeOut;
                toastr.options.closeButton = true;
                toastr["success"]("Les adhérents ont été fusionnés " + data.substring(5));
                toastr.options.timeOut = toasterOptionTimeout;
                toastr.options.closeButton = toasterOptionCloseButton;
                if (callback) {
                    callback(piIdAdherentAConserver, piIdAdherentAVirer);
                }
            } else {
                var toasterOptionTimeout = toastr.options.timeOut;
                toastr.options.timeOut = 0;
                toastr["error"]("Une erreur est survenue : " + data);
                toastr.options.timeOut = toasterOptionTimeout;
            }
        })
        .fail(function (error) {
            var toasterOptionTimeout = toastr.options.timeOut;
            toastr.options.timeOut = 0;
            toastr["error"]("Une erreur est survenue : " + error.responseText);
            toastr.options.timeOut = toasterOptionTimeout;
        });
    if (event) {
        event.preventDefault();
    }
}


function envoyerMDP(event) {
    var idAdherent = $("#idAdherent").val();
    if (!idAdherent) {
        // Adhérent pas encore enregistré
        alert("Il faut enregistrer l'adhérent avant d'envoyer le mail");
        return;
    }
    $.post("envoi-MDP.php", {idAdherent: idAdherent})
            .done(function (data) {
                if (data && data === 'true') {
                    var toasterOptionCloseButton = toastr.options.closeButton;
                    var toasterOptionTimeout = toastr.options.timeOut;
// LG 20200419 deac                                                                    toastr.options.timeOut = 0;
                    toastr.options.closeButton = true;
                    toastr["success"]("Le mail a été envoyé");
                    toastr.options.timeOut = toasterOptionTimeout;
                    toastr.options.closeButton = toasterOptionCloseButton;
                } else {
                    toastr["error"]("Une erreur est survenue : " + data);
                }
            })
            .fail(function (error) {
//						debugger ;
                toastr["error"]("Une erreur est survenue : " + error.responseText);
            });
    event.preventDefault();
}

$(document).ready(function () {
    $('#select2-TAGS').select2({placeholder: "Choisissez les tags de cet adhérent"});
    $('#select2-ROLES').select2({placeholder: "Choisissez les rôles de cet adhérent"});
});
