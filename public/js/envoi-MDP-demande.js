// notification d'ajout d'une activite          
var form = $('#envoi-MDP-demande');
form.submit(function (event) {
    var lsMail = $("#mail").val();
    var lsCaptcha = $("#captcha").val();
    if (!lsMail) {
        // Mail non saisi
        alert("Il faut indiquer votre adresse mail");
        return;
    }
    if (!lsCaptcha) {
        // Captcha non saisi
        alert("Il faut indiquer le nom de votre association pour prouver que vous n'êtes pas un robot");
        return;
    }
    
    $.post("envoi-MDP.php", {mail: lsMail, captcha: lsCaptcha})
            .done(function (data) {
                if (data && data === 'true') {
                    var toasterOptionCloseButton = toastr.options.closeButton;
                    var toasterOptionTimeout = toastr.options.timeOut;
                    var toasterOptionOnHidden = toastr.options.onHidden;
                    toastr.options.closeButton = true;
                    toastr.options.onHidden = function() { 
                        window.location.href = "index.php" ;
                    }
                    toastr["success"]("Le mail contenant votre nouveau mot de passe a été envoyé (pensez à regarder dans vos SPAMS!).<br>Vous allez être redirigé vers la page d'accueil dans quelques instants");
                    toastr.options.timeOut = toasterOptionTimeout;
                    toastr.options.closeButton = toasterOptionCloseButton;
                    toastr.options.onHidden = toasterOptionOnHidden;
// debugger ;  // Il faut rediriger les la page d'accueil
                } else {
                    toastr["error"]("Une erreur est survenue : " + data);
                }
            })
            .fail(function (error) {
                toastr["error"]("Une erreur est survenue : " + error.responseText);
            });
    event.preventDefault();
});


        
