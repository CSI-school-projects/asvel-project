document.getElementById('previous-page').addEventListener('click', function() {
    if (currentPage > 0) {
        currentPage--;
        updatePage();
    }
    // Mettre à jour l'état du bouton "Précédent"
    if (currentPage == 0) {
        this.disabled = true;
    } else {
        this.disabled = false;
    }
    // Mettre à jour l'état du bouton "Suivant"
    document.getElementById('next-page').disabled = false;
    // Mettre à jour le compteur de pages
    updateCounter();
  });
  
document.getElementById('next-page').addEventListener('click', function() {
    if (currentPage < totalPages - 1) {
        currentPage++;
        updatePage();
    }
    // Mettre à jour l'état du bouton "Suivant"
    if (currentPage === totalPages - 1) {
        this.disabled = true;
    } else {
        this.disabled = false;
    }
    // Mettre à jour l'état du bouton "Précédent"
    document.getElementById('previous-page').disabled = false;
    // Mettre à jour le compteur de pages
    updateCounter();
  });
  
function updatePage() {
    document.querySelector('.card-body').innerHTML = ''; // Supprimez les éléments existants
    // Mettre à jour les éléments de la page en cours
    for (var i = 0; i < itemsPerPage; i++) {
        var itemIndex = currentPage * itemsPerPage + i;
        var item = data[itemIndex];
        var figureEl = document.createElement('figure');
        figureEl.classList.add('myFigure');
        figureEl.innerHTML = '<a href="'+ item['url'] + '"><img src="' + item['image_src'] + '" width=15% height=15% /></a><figcaption>' + item['caption'] + '</figcaption>';
        document.querySelector('.card-body').appendChild(figureEl);
    }
}
  
function updateCounter() {
    // Mettre à jour le compteur de pages
    document.querySelector('#pagination-controls span').innerHTML = 'Page ' + (currentPage + 1) + ' sur ' + totalPages;
}
    
// Mettre à jour les éléments de la page en cours au chargement de la page
updatePage();