<?php

use App\Repository\MaterielRepository;

require_once '../../config/globalConfig.php';

if (isset($_POST['materiel'])){
    $materiel = [
    'tailleMateriel' => $_POST['tailleMateriel'],
    'pointureMateriel' => $_POST['pointureMateriel'],
    'modeleMateriel' => $_POST['modeleMateriel'],
    'dateachatMateriel' => date('d.m.Y, H:i:s'),
    'commentaireMateriel' => $_POST['commentaireMateriel'],
    'idAdherent' => $_POST['select'],
    'idTypeMateriel' => $_POST['select1'],
    'idTypeMiseADispo' => $_POST['select2'],
    ];

    $newMateriel = new MaterielRepository();

    $newMateriel->createEntite($materiel);
    header("location: liste-materiel.php");
}