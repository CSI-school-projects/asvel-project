<!-- Footer -->
<?php

use App\Security;
?>
<footer class="page-footer font-small special-color-dark sticky-footer" id="footer_info">
    <div class="container py-2">
        <div class="row justify-content-center">
            <?php
            if (isset($valider) && $valider) {
                echo '<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Valider</button>';
            }
            ?>
            <?php
            if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
                if (isset($liste)) {
                    echo ' <a style="margin-left: 1%;" class="btn btn-primary" href="liste-' . $liste . '</a> ';
                }
                if (isset($footer_listeBoutons)) {
                    foreach ($footer_listeBoutons as $bouton) {
                        echo "&nbsp;" . $bouton;
                    }
                }
            }
            ?>
        </div>
    </div>
</footer>
<script>
    // LG 20210123 début
    // Avertir l'utilisateur s'il change de page sans avoir validé
    // Selon https://stackoverflow.com/questions/11844256/alert-for-unsaved-changes-in-form
    // Store form state at page load
    var initial_form_state = $('form').not('.login-form').not('#formAdherentModal').serialize();

    // Store form state after form submit
    $('form').submit(function () {
        initial_form_state = $('form').not('.login-form').not('#formAdherentModal').serialize();
    });

    // Check form changes before leaving the page and warn user if needed
    $(window).bind('beforeunload', function (e) {
        var form_state = $('form').not('.login-form').not('#formAdherentModal').serialize();
        if (initial_form_state != form_state) {
            var message = "Cette page contient des modifications non enregistrées. Souhaitez-vous quitter la page et perdre vos modifications, ou quitter la page ?";
            e.returnValue = message; // Cross-browser compatibility (src: MDN)
            return message;
        }
    });
    // LG 20210123 fin
</script>

<?php
/* LG 20210513 deac
<script>
    var unsaved = false;

    $(":input").change(function () { //triggers change in all input fields including text type
        unsaved = true;
    });

    function unloadPage() {
        if (unsaved) {
            return "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
        }
    }

    window.onbeforeunload = unloadPage;
</script>
 */
 ?>