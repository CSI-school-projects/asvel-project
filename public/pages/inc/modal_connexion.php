<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLoginLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLoginLabel">Connexion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <span class="login-error">Email/Mot de passe incorrect</span>
            <form class="login-form">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Email :</label>
                        <input type="text" class="form-control" id="username" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Mot de passe :</label>
                        <input type="password" class="form-control" id="password" name="password" maxlength="50" required>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="submit" class="btn btn-primary" id="login-form-submit">Connexion</button>
                </div>
                <div class="modal-footer">
                    <a href="" id="login-form-newpwd" title="Si vous avez saisi la même adresse mail que celle que vous avez fournie lors de votre inscription, un mail vous sera adressé avec votre nouveau mot de passe">M'envoyer un nouveau mot de passe</a>
                </div>
            </form>
        </div>
    </div>
</div>
