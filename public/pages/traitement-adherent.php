<?php

require_once '../../config/globalConfig.php';

use App\Entity\Adherent;
use App\Repository\AdherentRepository;
use App\Security;
use \App\Lib\Functions;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header("location: Accessdenied.php");
}
// echo($_POST['nomAdherent']);

$idAdherent = filter_input(INPUT_POST, 'idAdherent', FILTER_SANITIZE_STRING);
$nomAdherent = filter_input(INPUT_POST, 'nomAdherent', FILTER_SANITIZE_STRING);
$prenomAdherent = filter_input(INPUT_POST, 'prenomAdherent', FILTER_SANITIZE_STRING);
$dateNaissanceAdherent = filter_input(INPUT_POST, 'dateNaissanceAdherent', FILTER_SANITIZE_STRING);
$adresseAdherent = filter_input(INPUT_POST, 'adresseAdherent', FILTER_SANITIZE_STRING);
$adresse2Adherent = filter_input(INPUT_POST, 'adresse2Adherent', FILTER_SANITIZE_STRING);
$mailAdherent = filter_input(INPUT_POST, 'mailAdherent', FILTER_SANITIZE_STRING);
$telAdherent = filter_input(INPUT_POST, 'telAdherent', FILTER_SANITIZE_STRING);
$cpAdherent = filter_input(INPUT_POST, 'cpAdherent', FILTER_SANITIZE_STRING);
$villeAdherent = filter_input(INPUT_POST, 'villeAdherent', FILTER_SANITIZE_STRING);
$paysAdherent = filter_input(INPUT_POST, 'paysAdherent', FILTER_SANITIZE_STRING);
$sexeAdherent = filter_input(INPUT_POST, 'sexeAdherent', FILTER_SANITIZE_STRING);
$licenseAdherent = filter_input(INPUT_POST, 'licenseAdherent', FILTER_SANITIZE_STRING);
$droitImageAdherent = filter_input(INPUT_POST, 'droitImageAdherent', FILTER_SANITIZE_STRING);
$refusMailAdherent = filter_input(INPUT_POST, 'refusMailAdherent', FILTER_SANITIZE_STRING);
$idTypeReglementAdherent = filter_input(INPUT_POST, 'idTypeReglementAdherent', FILTER_SANITIZE_STRING);
$montantReglementAdherent = filter_input(INPUT_POST, 'montantReglementAdherent', FILTER_SANITIZE_STRING);
$commentaireAdherent = filter_input(INPUT_POST, 'commentaireAdherent', FILTER_SANITIZE_STRING);
$prenomContact = filter_input(INPUT_POST, 'prenomContact', FILTER_SANITIZE_STRING);
$nomContact = filter_input(INPUT_POST, 'nomContact', FILTER_SANITIZE_STRING);
$adresseContact = filter_input(INPUT_POST, 'adresseContact', FILTER_SANITIZE_STRING);
$adresse2Contact = filter_input(INPUT_POST, 'adresse2Contact', FILTER_SANITIZE_STRING);
$villeContact = filter_input(INPUT_POST, 'villeContact', FILTER_SANITIZE_STRING);
$cpContact = filter_input(INPUT_POST, 'cpContact', FILTER_SANITIZE_STRING);
$mailContact = filter_input(INPUT_POST, 'mailContact', FILTER_SANITIZE_STRING);
$telContact = filter_input(INPUT_POST, 'telContact', FILTER_SANITIZE_STRING);
$idTypeCertif = filter_input(INPUT_POST, 'idTypeCertif', FILTER_SANITIZE_STRING);
$certifNomMedecin = filter_input(INPUT_POST, 'certifNomMedecin', FILTER_SANITIZE_STRING);
$certifAlpi = filter_input(INPUT_POST, 'certifAlpi', FILTER_SANITIZE_STRING);
$saeAdherent = filter_input(INPUT_POST, 'saeAdherent', FILTER_SANITIZE_STRING);
$nationaliteAdherent = filter_input(INPUT_POST, 'nationaliteAdherent', FILTER_SANITIZE_STRING);
$roles = implode(",", filter_var_array(isset($_POST["roles"])?$_POST["roles"]:[]));
$tags = implode(",", filter_var_array(isset($_POST["tags"])?$_POST["tags"]:[]));
$idAutreaffiliation = filter_input(INPUT_POST, 'idAutreaffiliation', FILTER_SANITIZE_STRING);
$mdpAdherent = filter_input(INPUT_POST, 'mdpAdherent', FILTER_SANITIZE_STRING);

$AdherentRep = new AdherentRepository();
if($idAdherent != NULL){
    $adherentedit = $AdherentRep->getEntitesById($idAdherent);
} else {
    $adherentedit= new Adherent();
}
// LG 20220909 début : avec le code initial, on ne peut pas effacer une info dadhérent (par exemple, mail)
// $datas = array(
//     'idAdherent' => $idAdherent,
//     'nomAdherent' => $nomAdherent ?: $adherentedit->getNomAdherent(),
//     'prenomAdherent' => $prenomAdherent ?: $adherentedit->getPrenomAdherent(),
//     'dateNaissanceAdherent' => $dateNaissanceAdherent ?:$adherentedit->getDateNaissanceAdherent(),
//     'adresseAdherent' => $adresseAdherent ?:$adherentedit->getAdresseAdherent(),
//     'adresse2Adherent' => $adresse2Adherent ?:$adherentedit->getAdresse2Adherent(),
//     'mailAdherent' => $mailAdherent ?:$adherentedit->getMailAdherent(),
//     'telAdherent' => $telAdherent ?:$adherentedit->getTelAdherent(),
//     'cpAdherent' => $cpAdherent ?:$adherentedit->getCpAdherent(),
//     'villeAdherent' => $villeAdherent ?:$adherentedit->getVilleContact(),
//     'paysAdherent' => $paysAdherent ?:$adherentedit->getPaysAdherent(),
//     'nationaliteAdherent' => $nationaliteAdherent ?:$adherentedit->getNationaliteAdherent(),
//     'licenseAdherent' => $licenseAdherent ?:$adherentedit->getLicenseAdherent(),
//     'sexeAdherent' => $sexeAdherent ?:$adherentedit->getSexeAdherent(),
//     'droitImageAdherent' => $droitImageAdherent ?:$adherentedit->getDroitImageAdherent(),
//     'refusMailAdherent' => $refusMailAdherent ?:$adherentedit->getRefusMailAdherent(),
//    //    'idTypeReglementAdherent' => $idTypeReglementAdherent,
//    //    'montantReglementAdherent' => $montantReglementAdherent,
//     'commentaireAdherent' => $commentaireAdherent ?:$adherentedit->getCommentaireAdherent(),
//     'prenomContact' => $prenomContact ?:$adherentedit->getPrenomContact(),
//     'nomContact' => $nomContact ?:$adherentedit->getNomContact(),
//     'adresseContact' => $adresseContact ?:$adherentedit->getAdresseContact(),
//     'adresse2Contact' => $adresse2Contact ?:$adherentedit->getAdresse2Contact(),
//     'villeContact' => $villeContact ?:$adherentedit->getVilleContact(),
//     'cpContact' => $cpContact ?:$adherentedit->getCpContact(),
//     'mailContact' => $mailContact ?:$adherentedit->getMailContact(),
//     'telContact' => $telContact ?: $adherentedit->getTelContact(),
//     'idTypeCertif' => $idTypeCertif ?: $adherentedit->getIdTypeCertif(),
//     'certifNomMedecin' => $certifNomMedecin ?: $adherentedit->getCertifNomMedecin(),
//     'certifAlpi' => $certifAlpi ?:$adherentedit->getCertifAlpi(),
//     'saeAdherent' => $saeAdherent ?: $adherentedit->getSaeAdherent(),
//     'roles' => $roles,
//     'tags' => $tags,
//     'idAutreaffiliation' => $idAutreaffiliation ?: $adherentedit->getIdAutreaffiliation(),
// 	'mdpAdherent' => $mdpAdherent?($AdherentRep->crypteMDP($mdpAdherent)):$adherentedit->getMDPAdherent(),
//    // LG 20200214 : réactiver quand les passeports seront inclus dans EditAdherent.php    'passeports' => implode(",", filter_var_array ($_POST["passeports"])),
//    );
    if (!$mailAdherent) {
        // Mail adherent mis à vide -> le mettre à NULL pour éviter les doublons de vides
        $mailAdherent = null ;
    }
    $datas = array(
        'idAdherent' => $idAdherent,
        'nomAdherent' => $nomAdherent,
        'prenomAdherent' => $prenomAdherent,
        'dateNaissanceAdherent' => $dateNaissanceAdherent,
        'adresseAdherent' => $adresseAdherent,
        'adresse2Adherent' => $adresse2Adherent,
        'mailAdherent' => $mailAdherent,
        'telAdherent' => $telAdherent,
        'cpAdherent' => $cpAdherent,
        'villeAdherent' => $villeAdherent,
        'paysAdherent' => $paysAdherent,
        'nationaliteAdherent' => $nationaliteAdherent,
        'licenseAdherent' => $licenseAdherent,
        'sexeAdherent' => $sexeAdherent,
        'droitImageAdherent' => $droitImageAdherent,
        'refusMailAdherent' => $refusMailAdherent,
    //    'idTypeReglementAdherent' => $idTypeReglementAdherent,
    //    'montantReglementAdherent' => $montantReglementAdherent,
        'commentaireAdherent' => $commentaireAdherent,
        'prenomContact' => $prenomContact,
        'nomContact' => $nomContact,
        'adresseContact' => $adresseContact,
        'adresse2Contact' => $adresse2Contact,
        'villeContact' => $villeContact,
        'cpContact' => $cpContact,
        'mailContact' => $mailContact,
        'telContact' => $telContact,
        'idTypeCertif' => $idTypeCertif,
        'certifNomMedecin' => $certifNomMedecin,
        'certifAlpi' => $certifAlpi,
        'saeAdherent' => $saeAdherent,
        'roles' => $roles,
        'tags' => $tags,
        'idAutreaffiliation' => $idAutreaffiliation,
        'mdpAdherent' => $mdpAdherent?($AdherentRep->crypteMDP($mdpAdherent)):$adherentedit->getMDPAdherent(),
    // LG 20200214 : réactiver quand les passeports seront inclus dans EditAdherent.php    'passeports' => implode(",", filter_var_array ($_POST["passeports"])),
    );
// LG 20220909 fin

// var_dump($datas);

// LG 20200224 début
// Gestion des booleens : ils ne sont pas passés quand "False"
if (!$datas["certifAlpi"]) {
    $datas["certifAlpi"] = false;
}
if (!$datas["refusMailAdherent"]) {
    $datas["refusMailAdherent"] = false;
}
if (!$datas["droitImageAdherent"]) {
    $datas["droitImageAdherent"] = false;
}
if (!$datas["idAutreaffiliation"]) {
    $datas["idAutreaffiliation"] = NULL;
}
if (!$datas["dateNaissanceAdherent"]) {
    $datas["dateNaissanceAdherent"] = NULL;
}

// LG 20200224 fin
try{   
    $Adherent = new Adherent($datas);
    if ($AdherentRep->vérifier($Adherent, $tabErr)) {
        $AdherentSauvé = $AdherentRep->sauver($Adherent);
        if ($AdherentSauvé && !$idAdherent && isset($_POST["idAffiliation"])/**/) {
            // L'adhérent a été enregistré, et c'est un nouvel adhérent, t le formulaire contient les informations sur l'affiliation
            // Enregistrer aussi l'affiliation
            ob_start();     // Démarrer la récupération du flux de sortie
// echo 'toto' ;
            $traitement_affiliation_IdAdherent = $AdherentSauvé->getIdAdherent() ;
            require_once 'traitement-affiliation.php';
            $res = ob_get_contents() ;  // Récupérer le flux de sortie pour savoir si tout a bien fonctionné
            ob_clean () ;               // Vider le flux de sortie
// echo "|" . $res . "|" ;
            if (!is_numeric($res)) {
                // Une valeur non numérique indique une erreur
                echo "L'enregistrement de l'adhérent est OK, mais l'affiliation n'a pas pu être sauvegardée : " . $res ;
            }
        }
// echo "tata" ;
        echo $AdherentSauvé->getIdAdherent() ;
    } else {
        echo $tabErr[0];
    }
} catch(PDOException $e){
    echo $e->getMessage();
}


