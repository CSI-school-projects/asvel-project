<?php
require_once '../../config/globalConfig.php';
require_once '../../vendor/PDF/autoload.php';

use App\Repository\PhotoRepository as PR;
use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
    header('Location: Accessdenied.php');
    return;
}

$photoRepo = new PR();
$date = date('Y-m-d H:i:s');

if (isset($_FILES['file'])) {
    // Mise en mémoire des variables de l'image
    $tmpName = $_FILES['file']['tmp_name'];
    $name = $_FILES['file']['name'];
    $size = $_FILES['file']['size'];
    $error = $_FILES['file']['error'];

    $tabExtension = explode('.', $name);
    $extension = strtolower(end($tabExtension));

    // Tableau des extensions que l'on accepte
    $extensions = ['jpg', 'png', 'jpeg', 'gif'];

    if (in_array($extension, $extensions)) {
        $cheminPhoto = '../img/photos/'.$name;
        move_uploaded_file($tmpName, $cheminPhoto);
        $error = null;
        
        if (isset($_GET['idSortie'])) {
            $photoRepo->insertPhoto($date, $cheminPhoto, $_SESSION['idadherent'], $_GET['idSortie']);
        }
    } elseif ($error == 0) {
        $error = "Une erreur est survenue.";
    } else {
        $error = "Mauvaise extension. Extensions autorisées : jpg / png / jpeg / gif.";
    }
}

?>
<!DOCTYPE html>
<link rel="stylesheet" href="../css/datatables.css">

<html>
    <head>
        <title>Upload photo - ASVEL Ski Montagne</title>
        <?php include_once 'inc/head.php'; ?>
    </head>
    <body>
        <header>
            <?php include_once 'inc/header.php'; ?>
        </header>

        <div class="card text-center m-3">
            <h1 class="card-header"><?php if (isset($error)) { echo $error; } else { echo "Photo importée"; }?></h3>
        </div>

        <div class="card-body">
            <a href="index.php">
                <div>
                    <p>Revenir à la page d'acceuil</p>
                </div>
            </a>
        </div>

        <div class="card-footer pb-0 pt-3">

        </div>

        <?php include_once 'inc/footer.php'; ?>
    </body>
</html>