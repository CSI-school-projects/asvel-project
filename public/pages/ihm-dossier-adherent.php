<?php
require_once '../../config/globalConfig.php';
require_once '../../vendor/PDF/autoload.php';

use App\Entity\Adherent;
use App\Repository\AdherentRepository as AR;
use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
    header('Location: Accessdenied.php');
    return;
}

$adherentRepo = new AR();
$allAdPhoto = $adherentRepo->getAllWithPhoto();

// Récupérer les données à afficher dans la pagination
$data = [];

foreach ($allAdPhoto as $key => $ad) {
  $data[] = [
    'caption' => $ad['utilisateur'],
    'image_src' => '../img/Dossier.png',
    'url' => URL_BASE.'public/pages/ihm-photo-byAdherent.php?idadherent='.$ad['idadherent'],
  ];
}

// Définir le nombre d'éléments à afficher par page
$itemsPerPage = 20;
// Définir la variable $currentPage
$currentPage = 0;
// Déterminer le nombre total de pages
$totalPages = ceil(count($data) / $itemsPerPage);

?>
<!DOCTYPE html>
<link rel="stylesheet" href="../css/datatables.css">
<link rel="stylesheet" href="../css/ihm_photo.css">

<html>
  <head>
    <title>Photos des sorties - ASVEL Ski Montagne</title>
    <?php include_once 'inc/head.php'; ?>
  </head>
  <body>
    <header>
      <?php include_once 'inc/header.php'; ?>
    </header>

    <div class="card text-center m-3">
      <h1 class="card-header">Dossiers des adhérents</h3>
    </div>

    <div class="card-body">

    </div>

    <div class="card-footer pb-0 pt-3">
      <!-- Afficher les contrôles de pagination -->
      <div id="pagination-controls">
        <button id="previous-page" class="btn btn-secondary">Précédent</button>
        <span>Page <?php echo $currentPage + 1; ?> sur <?php echo $totalPages; ?></span>
        <button id="next-page" class="btn btn-secondary">Suivant</button>
      </div>
    </div>

    <?php include_once 'inc/footer.php'; ?>

    <!-- Récupération des variables en JavaScript -->
    <script>
      // Récupérer le nombre total de pages
      var totalPages = <?php echo $totalPages; ?>;
      // Récupérer la page en cours
      var currentPage = <?php echo $currentPage; ?>;
      // Récupérer les données à afficher dans la pagination
      var data = <?php echo json_encode($data); ?>;
      // Récupérer le nombre d'éléments à afficher par page
      var itemsPerPage = <?php echo $itemsPerPage; ?>;
    </script>

    <script src="../js/pagination.js"></script>
  </body>
</html>
