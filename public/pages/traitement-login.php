<?php

use App\Entity\Adherent;
use App\Repository\AdherentRepository;
use App\Security;

require_once '../../config/globalConfig.php';
// IG28012021
// $mail = strtolower($_POST['mail']);
$mail = strtolower(filter_input(INPUT_POST, 'mail', FILTER_SANITIZE_STRING));


// IG 28012021 Debut
// $password = $_POST['password'];
// IG 28012021 Fin

$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

$adh = new Adherent(array(
    'mailadherent' => $mail,
    'mdpadherent' => $password
));
$repo = new AdherentRepository();

$auth = $repo->auth($adh);
// $auth = true ;
echo json_encode($auth);

if (!Security::hasRole(Array(Security::ROLE_SECRETAIRE, Security::ROLE_ENCADRANT, Security::ROLE_ADMIN))) {
// LG 20200224 déac    header("location: Accessdenied.php");
}
