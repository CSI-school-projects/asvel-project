<?php

require_once '../../config/globalConfig.php';

use App\Entity\Participer;
use App\Repository\ParticiperRepository;
use App\Security;

$idSortie = filter_input(INPUT_POST, 'idsortie', FILTER_SANITIZE_NUMBER_INT);
$idAdherent = filter_input(INPUT_POST, 'idadherent', FILTER_SANITIZE_NUMBER_INT);
$idSortie_Initial = filter_input(INPUT_POST, 'idSortieInitial', FILTER_SANITIZE_NUMBER_INT);
$idAdherent_Initial = filter_input(INPUT_POST, 'idParticipantInitial', FILTER_SANITIZE_NUMBER_INT);
if (!$idSortie_Initial)
	$idSortie_Initial = $idSortie;
if (!$idAdherent_Initial)
	$idAdherent_Initial = $idAdherent;
// LG 20200416 début
if (!$idAdherent)
	$idAdherent = $idAdherent_Initial;
if (!$idSortie || !$idAdherent) {
	// Les informations sont incomplètes
	echo "Il manque l'information sur la sortie ou sur le participant.<br>";
	return false;
}
// LG 20200416 fin
// $nomParticipant = filter_input(INPUT_POST, 'nomparticipant', FILTER_SANITIZE_STRING);
$fonctionParticipant = filter_input(INPUT_POST, 'fonction', FILTER_SANITIZE_STRING);
$dateInscription = filter_input(INPUT_POST, 'dateinscription', FILTER_SANITIZE_STRING);
$idInscripteur = filter_input(INPUT_POST, 'idinscripteur', FILTER_SANITIZE_STRING);
$montantArrhes = filter_input(INPUT_POST, 'montantarrhes', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
$montantTotal = filter_input(INPUT_POST, 'montanttotal', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
$idTypeReglementArrhes = filter_input(INPUT_POST, 'idtypereglementarrhes', FILTER_SANITIZE_NUMBER_INT);
$idTypeReglementMontantTotal = filter_input(INPUT_POST, 'idtypereglementmontanttotal', FILTER_SANITIZE_NUMBER_INT);
$arrhesRemboursees = filter_input(INPUT_POST, 'arrhesRemboursees', FILTER_SANITIZE_STRING);
$idRaisonAnnulation = filter_input(INPUT_POST, 'idraisonannulation', FILTER_SANITIZE_NUMBER_INT);
$commentaire = filter_input(INPUT_POST, 'commentaireParticipation', FILTER_SANITIZE_STRING);
$validationparticipation = filter_input(INPUT_POST, 'validationparticipation', FILTER_SANITIZE_STRING);
$Debutant = filter_input(INPUT_POST, 'debutant', FILTER_SANITIZE_STRING);
$LoueMateriel = filter_input(INPUT_POST, 'louemateriel', FILTER_SANITIZE_STRING);
$DVA = filter_input(INPUT_POST, 'dva', FILTER_SANITIZE_STRING);
$PelleSonde = filter_input(INPUT_POST, 'pellesonde', FILTER_SANITIZE_STRING);
$nbPlaceVoiture = filter_input(INPUT_POST, 'nbPlaceVoiture', FILTER_SANITIZE_NUMBER_INT);
$ExcluDuCovoiturage = filter_input(INPUT_POST, 'ExcluDuCovoiturage', FILTER_SANITIZE_STRING);
$datas = array(
	'idAdherent_Initial' => $idAdherent_Initial,
	'idSortie_Initial' => $idSortie_Initial,
	'idAdherent' => $idAdherent,
	'idSortie' => $idSortie,
	'idFonction' => $fonctionParticipant ?: 1,
	'dateinscription' => $dateInscription ?: Date("Y-m-d H:i"), //Date("Y-m-d H:i:s"),
	'idInscripteur' => $idInscripteur,
	'montantArrhes' => $montantArrhes ?: 0,
	'montanttotal' => $montantTotal ?: 0,
	'idtypereglementarrhes' => $idTypeReglementArrhes ?: null,
	'idtypereglementmontanttotal' => $idTypeReglementMontantTotal ?: null,
	'arrhesRemboursees' => ($arrhesRemboursees) ?: FALSE,
	'idRaisonAnnulation' => $idRaisonAnnulation ?: Null,
	'commentaireparticipation' => $commentaire,
	'validationparticipation' => ($validationparticipation || $fonctionParticipant == 2) ?: FALSE,
	'debutant' => $Debutant ?: FALSE,
	'louemateriel' => $LoueMateriel ?: FALSE,
	'DVA' => $DVA ?: FALSE,
	'PelleSonde' => $PelleSonde ?: FALSE,
	'nbPlaceVoiture' => $nbPlaceVoiture ?: 0,
	'ExcluDuCovoiturage' => $ExcluDuCovoiturage ?: FALSE,
);

// LG 20200416 début
// Vérifier les droits
if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
	// RAS
} else if (Security::hasRole(Array(Security::ROLE_ADHERENT)) && isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $idAdherent) {
	// C'est le participant lui-même : il a le droit de s'inscrire sur les sorties qui l'autorisent, mais ne peut rien changer, sauf le nb de places de voiture
	$sortieRepo = new App\Repository\SortieRepository();
	$sortie = $sortieRepo->getEntityById($idSortie);
	if (!$sortie->getInscriptionEnLigne()) {
		echo "L'inscription en ligne n'est pas disponible pour cette sortie";
		return false;
	}
	// Il y a une faille de sécurité ici : l'adhérent pourrait envoyer (en post) des données qu'in n'a pas le droit de modifier : ca reste à corriger   
} else {
//    header("location: Accessdenied.php");
	echo 'Désolé, vous ne disposez pas des droits suffisants pour effectuer cette action';
	return false;
}
// LG 20200416 fin
// Enregistrer
try {
	$ParticiperRep = new ParticiperRepository();
	$Participer = new Participer($datas);
	
// echo $Participer->getMontantTotal() ;
	
	
	$Participer->setIdAdherent_Initial($idAdherent_Initial);
	$Participer->setIdSortie_Initial($idSortie_Initial);

	if ($ParticiperRep->vérifier($Participer, $tabErr)) {
		$LaParticipation = $ParticiperRep->sauver($Participer);
		echo 'true';
	} else {
		echo $tabErr[0];
	}
} catch (PDOException $e) {
	echo $e->getMessage();
}
