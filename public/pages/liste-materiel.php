<?php

use App\Repository\AdherentRepository;
use App\Repository\MaterielRepository;
use App\Repository\TypeMaterielRepository;
use App\Repository\TypeMiseADispoRepository;
use App\Security;

ob_start();
require_once '../../config/globalConfig.php';

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header('Location: Accessdenied.php');
    //    echo "Merci de vous authentifier pour accéder à cette fonctionnalité" ;
    return;
}

//instanciation repo
$materiel = new MaterielRepository();
$tmap = new TypeMiseADispoRepository();
$proprio = new AdherentRepository();
$tm = new TypeMaterielRepository();

//fonctions getAll()
$Desmateriels = $materiel->getAll();
$Destmap = $tmap->getAll();
$Desproprio = $proprio->getAll();
$Destm = $tm->getAll();
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/css/Materiel.css">

<html>

<body>
    <?php
    include_once 'inc/header.php';
    ?>

    <div class="row py-2"></div>
    <h1 style='text-align: center;font-size: 30px;'> Materiel </h1>

    <?php
    if (isset($_POST['date']) && !($_POST['date'] == "")) {
        $date = $_POST['date'];
        var_dump($date);

        foreach ($Desmateriels as $value) {
            $dateRepo = $value->getDateAchatMateriel(); //var_dump($dateRepo);var_dump($value);

            echo strcmp($date, $dateRepo);

            /*if($date == $dateRepo){
        $res = $value->getIdMateriel();
        return $res;var_dump($res);
    }else{
        $res = 'La date ne correspond à aucun materiel';
        return $res;var_dump($res);
    }
    var_dump($res);*/
        }

        //page ajout de materiel
    } elseif (isset($_POST['ajout'])) {
    ?>
        <article>
            <div class="row py-2"></div>
            <form method="post" action="ajoutMateriel.php">
                <label>Taille du materiel</label>
                <input type="number" name="tailleMateriel">
                <label>Pointure du materiel</label>
                <input type="number" name="pointureMateriel"><br>

                <label>Modèle du materiel</label>
                <input type="text" name="modeleMateriel">
                <label>Commentaire</label>
                <input type="text" name="commentaireMateriel"><br>

                <label>Propriétaire</label>
                <select name="select">
                    <option value="">--Choisir un proprio--</option>
                    <?php
                    foreach ($Desproprio as $proprio) { ?>
                        <option value="<?= $proprio->getIdAdherent(); ?>">
                        <?= $proprio->getNomAdherent() . '<br>';
                    }
                        ?>
                        </option>
                </select><br>

                <label>Type du Materiel</label>
                <select name="select1">
                    <option value="">--Choisir un materiel--</option>
                    <?php
                    foreach ($Destm as $valtm) { ?>
                        <option value="<?= $valtm->getIdTypeMateriel(); ?>">
                        <?= $valtm->getNomTypeMateriel() . '<br>';
                    }
                        ?>
                        </option>
                </select><br>

                <label>Mise à disposition du materiel</label>
                <select name="select2">
                    <option value="">--Mise à disposition--</option>
                    <?php
                    foreach ($Destmap as $valtmap) { ?>
                        <option value="<?= $valtmap->getIdTypeMiseADispo(); ?>">
                        <?= $valtmap->getNomTypeMiseADispo() . '<br>';
                    }
                        ?>
                        </option>
                </select><br>

                <input type="submit" name="materiel">
            </form>
        </article>
    <?php
    }
    //page en ayant choisi un type de materiel
    elseif (isset($_POST['select']) && !($_POST['select'] == "")) {
        $select = $_POST['select'];
    ?>
        <article>
            <div class="row py-2"></div>
            <form method="POST" action="#" id="myForm">
                <input type="date" class="date-1" name="date" onchange="$('#myForm').submit();">
            </form>
            <form method="POST" action="#" id="myForm2">
                <select name="select" onchange="$('#myForm2').submit();">
                    <option value="">--Choisir un materiel--</option>
                    <?php
                    foreach ($Destm as $valtm) { ?>
                        <option value="<?= $valtm->getIdTypeMateriel(); ?>">
                        <?= $valtm->getNomTypeMateriel() . '<br>';
                    }
                        ?>
                        </option>
                </select>
            </form>

            <table class="table table-hover" id="maTable">
                <thead class="thead-dark">
                    <th>Type de materiel
                        <input type="button" onclick="sortTable('maTable', 0, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 0, DESC)" value="↓">
                    </th>
                    <th>Propriétaire
                        <input type="button" onclick="sortTable('maTable', 1, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 1, DESC)" value="↓">
                    </th>
                    <th>Mode de mise à disponibilité
                        <input type="button" onclick="sortTable('maTable', 2, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 2, DESC)" value="↓">
                    </th>
                    <th>Commentaire
                        <input type="button" onclick="sortTable('maTable', 3, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 3, DESC)" value="↓">
                    </th>
                    <th>Taille
                        <input type="button" onclick="sortTable('maTable', 4, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 4, DESC)" value="↓">
                    </th>
                    <th>Modèle
                        <input type="button" onclick="sortTable('maTable', 5, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 5, DESC)" value="↓">
                    </th>
                </thead>
                <tr>
                    <?php
                    foreach ($Desmateriels as $value) {
                        $tm = $value->getIdTypeMateriel();

                        $unMateriel = $materiel->getByTypeMateriel($tm);
                        foreach ($unMateriel as $value) {
                            foreach ($Destmap as $valtmap) {
                                foreach ($Desproprio as $valproprio) {

                                    //if
                                    if ($valtmap->getIdTypeMiseADispo() == $value->getIdTypeMiseADispo()) {
                                        if ($valproprio->getIdAdherent() == $value->getIdAdherent()) {
                                            if ($tm == $value->getIdTypeMateriel()) {
                                                if ($tm == $select) {
                    ?>

                                                    <td><?= $valtm->getNomTypeMateriel(); ?></td>
                                                    <td><?= $valproprio->getNomAdherent(); ?></td>
                                                    <td><?= $valtmap->getNomTypeMiseADispo(); ?></td>
                                                    <td><?= $value->getCommentaireMateriel(); ?></td>
                                                    <td><?= $value->getTailleMateriel(); ?></td>
                                                    <td><?= $value->getModeleMateriel(); ?></td>
                </tr>
            <?php } else {
                                                    echo "Il n'y a pas de materiel"; ?>
                <form method="POST" action="#" id="myForm3">
                    <input type="submit" value="toto" onshow="$('#myForm3').submit();">
                </form> <?php
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }



                        ?>
            </table>
        </article>
    <?php
    }

    //page de base
    else {
    ?>
        <article>
            <div class="row py-2"></div>
            <form method="POST" action="#" id="myForm">
                <input type="date" class="date-1" name="date" onchange="$('#myForm').submit();">
            </form>
            <form method="POST" action="#" id="myForm2">
                <select name="select" onchange="$('#myForm2').submit();">
                    <option value="">--Choisir un materiel--</option>
                    <?php
                    foreach ($Destm as $valtm) { ?>
                        <option value="<?= $valtm->getIdTypeMateriel(); ?>">
                        <?= $valtm->getNomTypeMateriel() . '<br>';
                    }
                        ?>
                        </option>
                </select>
            </form>

            <!-- ajout de materiel -->
            <form method="post" action="#">
                <input type="submit" name="ajout" value="Ajouter un materiel">
            </form>

            <table class="table table-hover" id="maTable">
                <thead class="thead-dark">
                    <th>Type de materiel
                        <input type="button" onclick="sortTable('maTable', 0, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 0, DESC)" value="↓">
                    </th>
                    <th>Propriétaire
                        <input type="button" onclick="sortTable('maTable', 1, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 1, DESC)" value="↓">
                    </th>
                    <th>Mode de mise à disponibilité
                        <input type="button" onclick="sortTable('maTable', 2, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 2, DESC)" value="↓">
                    </th>
                    <th>Commentaire
                        <input type="button" onclick="sortTable('maTable', 3, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 3, DESC)" value="↓">
                    </th>
                    <th>Taille
                        <input type="button" onclick="sortTable('maTable', 4, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 4, DESC)" value="↓">
                    </th>
                    <th>Modèle
                        <input type="button" onclick="sortTable('maTable', 5, ASC)" value="↑">
                        <input type="button" onclick="sortTable('maTable', 5, DESC)" value="↓">
                    </th>
                </thead>
                <tr>
                    <?php //boucles

                    foreach ($Desmateriels as $valmateriel) {
                        foreach ($Destmap as $valtmap) {
                            foreach ($Desproprio as $valproprio) {
                                foreach ($Destm as $valtm) {

                                    //if
                                    if ($valtmap->getIdTypeMiseADispo() == $valmateriel->getIdTypeMiseADispo()) {
                                        if ($valproprio->getIdAdherent() == $valmateriel->getIdAdherent()) {
                                            if ($valtm->getIdTypeMateriel() == $valmateriel->getIdTypeMateriel()) {
                    ?>

                                                <td><?= $valtm->getNomTypeMateriel(); ?></td>
                                                <td><?= $valproprio->getNomAdherent(); ?></td>
                                                <td><?= $valtmap->getNomTypeMiseADispo(); ?></td>
                                                <td><?= $valmateriel->getCommentaireMateriel(); ?></td>
                                                <td><?= $valmateriel->getTailleMateriel(); ?></td>
                                                <td><?= $valmateriel->getModeleMateriel(); ?></td>
                </tr>
<?php }
                                        }
                                    }
                                }
                            }
                        }
                    } ?>
            </table>
        </article>
    <?php } ?>
    <!-- LG 20211127 déac <script src ="../js/login.js?v=<?= VERSION_APP ?>">-->
    <?php include_once 'inc/footer.php' ?>
    </style>
    <title>Table sorter</title>
    <script type='text/javascript'>
        function DESC(a, b) {
            a = a[1]
            b = b[1]
            if (a > b)
                return -1
            if (a < b)
                return 1
            return 0
        }

        function ASC(a, b) {
            a = a[1]
            b = b[1]
            if (a > b)
                return 1
            if (a < b)
                return -1
            return 0
        }

        function sortTable(tid, col, ord) {
            mybody = document.getElementById(tid).getElementsByTagName('tbody')[0]
            lines = mybody.getElementsByTagName('tr')
            var sorter = new Array()
            sorter.length = 0
            var i = -1;
            while (lines[++i]) {
                sorter.push([lines[i], lines[i].getElementsByTagName('td')[col].innerHTML])
            }
            sorter.sort(ord)
            j = -1
            while (sorter[++j]) {
                mybody.appendChild(sorter[j][0])
            }
        }
    </script>
</body>

</html>