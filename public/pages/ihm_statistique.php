<?php
use App\Repository\ActiviteRepository;
use App\Statistique;
use App\Security;
use App\date;
use Phaln\BDD;
// ob_start() ;
require_once '../../config/globalConfig.php';

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header('Location: Accessdenied.php');
    return;
}


$date = new date;
$plusanciennedate = $date -> plus_ancienne_date();


$activite = isset($_GET['act']) ? $_GET['act'] : 'toutes';
$datedebut = isset($_GET['debut']) ? $_GET['debut'] : $plusanciennedate;
$datefin = isset($_GET['fin']) ? $_GET['fin'] : date("Y-m-d");

$Stat = new Statistique;

$nbAdhenrents = $Stat -> nbAdherents($activite, $datedebut, $datefin);
$nbSorties = $Stat -> nbSorties($activite, $datedebut, $datefin);
$nbAdherentsMoyen = $Stat -> nbAdherentsMoyen($activite, $datedebut, $datefin);

$beneficeSortie = $Stat -> beneficeSortie($activite, $datedebut, $datefin);
$beneficeSortieTotal = $beneficeSortie['total'];
$beneficeSortieMoyen = $beneficeSortie['moyenne'];

$nbParticipations = $Stat -> nbParticipations($activite, $datedebut, $datefin);
$nbSortiesAnnulees = $Stat -> nbSortiesAnnulees($activite, $datedebut, $datefin);
$nbDesistements = $Stat -> nbDesistements($activite, $datedebut, $datefin);
$nbSortiesParAdherentTous = $Stat -> nbSortiesParAdherentTous($activite, $datedebut, $datefin);
$nbSortiesParAdherentRestreint = $Stat -> nbSortiesParAdherentRestreint($activite, $datedebut, $datefin);

?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/css/Stat.css">


<html>
    <body>

<?php
include_once 'inc/header.php';
?>

        <div class="row py-2"></div>
        <h1 style='text-align: center;font-size: 30px;'> Statistiques </h1>
        <article>
        <div class="row py-2"></div>
            <p>
                <form method='get'>
                    <select name="act" id="act">
                        <?php $activite = isset($_GET['act']) ? $_GET['act'] : 'toutes';
                            if($activite == 'toutes'){
                                echo "<option value ='toutes'>Toutes les activités</option>";
                            } else {
                                echo "<option selected value ='{$activite}'>{$activite} </option>";
                            }?>
                        <option value ='toutes'>Toutes les activités</option>
                        <?php 
                            $mapper = new ActiviteRepository();
                            $repo = $mapper -> getAll();
                            foreach ($repo as $value){
                                $nom = $value->getNomActivite();
                                echo "<option value ='{$nom}'>{$nom}</option>";
                            }
                        ?>
                    </select>
                    <label for="debut">Date de début</label> : <input type="date" name="debut" id="debut" value="<?php echo $datedebut; ?>"/>
                    <label for="fin">Date de fin</label> : <input type="date" name="fin" id="fin" value="<?php echo $datefin; ?>"/>
                    <input type="submit" value="afficher les statistiques"/>
                </form>
                <a href="ihm_statistique.php"><mark>Valeurs par défaut</mark></a>
            </p>

            <div class="row py-1"></div>

            <p>
                <strong>
                    <?php 
                        if ($activite == "Ski Alpin"){
                            echo "Statistiques pour toutes les activités : ";
                        }
                    ?>
                </strong>
            </p>

            <table class="table table-hover">
                <thead class="thead-dark">
                    <th>Nombre total d'adhérents <br/> participant aux sorties</th>
                    <th>Nombre total de sorties</th>
                    <th>Nombre moyen d'adhérents par sortie</th>
                    <th>Bénéfice/déficit moyen par sortie</th>
                    <th>Bénéfice/déficit total</th>
                </thead>
                <tr>
                    <td><?php echo $nbAdhenrents ?></td>
                    <td><?php echo $nbSorties ?></td>
                    <td><?php echo $nbAdherentsMoyen ?></td>
                    <td><?php echo $beneficeSortieMoyen ?></td>
                    <td><?php echo $beneficeSortieTotal ?></td>
                </tr>
                <thead class="thead-dark">
                    <th>Nombre de participations par adhérent </th>
                    <th>Nombre de sorties annulées</th>
                    <th>Nombre de désistements aux sorties </th>
                    <th>Nombre moyen de sorties par adhérent<br/>(tous adherents)</th>

                    <th>Nombre moyen de sorties par adhérent<br/>(uniquement les adherents ayant participé à des sorties)</th>
                </thead>
                <tr>
                    <td><?php echo $nbParticipations ?></td>
                    <td><?php echo $nbSortiesAnnulees ?></td>
                    <td><?php echo $nbDesistements ?></td>
                    <td><?php echo $nbSortiesParAdherentTous ?></td>
                    <td><?php echo $nbSortiesParAdherentRestreint ?></td>
                </tr>
            </table>

        </article>

        <!-- LG 20211127 déac <script src ="../js/login.js?v=<?= VERSION_APP ?>">-->
        <?php include_once 'inc/footer.php' ?>
    </body>

</html>