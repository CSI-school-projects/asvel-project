<?php

use App\Repository\RoleRepository;
use App\Repository\AdherentRepository;
use App\Repository\TypevilleContactRepository;
use App\Repository\TypeReglementRepository;
use App\Repository\FederationRepository;
use App\Repository\TagsRepository;
use App\Repository\PasseportRepository;
use App\Repository\TypeCertificatRepository;
use App\Entity\Adherent;
use App\Entity\Role;
use App\Entity\TypeReglement;
use App\Entity\Federation;
use App\Entity\Tag;
use App\Entity\Passeport;
use App\Security;
use App\Entity\TypeCertificat;

$AdherentRep = new AdherentRepository();
if (!isset($_GET['id'])) {

    $adherentedit = new Adherent([]);
} else {
    // IG 28012021 Debut
    // $adherentedit = $AdherentRep->getEntitesById($_GET["id"]);
    // IG 28012021 Fin
    $adherentedit = $AdherentRep->getEntitesById(filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING));
}

if (Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
    // L'utilisateur en cours a les droits sur les données
} else if (isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $adherentedit->getIdAdherent()) {
    // L'utilisateur en cours lui-même est en train de visualiser ses propres informations : RAS
} else {
    // Pas les droits
    header("location: Accessdenied.php");  
}

$disabled = "";
if (!Security::hasRole(Array(Security::ROLE_SECRETAIRE))) {
    $disabled = 'disabled="disabled" ';
}
if(isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $adherentedit->getIdAdherent() && Security::hasRole(Array(Security::ROLE_ADHERENT) )){
     $disabled = 'disabled="disabled" ';
}
if (Security::hasRole(Array(Security::ROLE_ENCADRANT))) {
    $disabled = '';
}
$lbAutoriseDataEtContacts = false;
if (Security::hasRole(Array(Security::ROLE_SECRETAIRE)) || (isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $adherentedit->getIdAdherent())) {
    $lbAutoriseDataEtContacts = true;
}

$lbAutoriseEcrireInfos = false;
if (Security::hasRole(Array(Security::ROLE_SECRETAIRE)) || (isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $adherentedit->getIdAdherent())) {
    $lbAutoriseEcrireInfos = true;
}

$lbAutoriseAdministrationDesDroits = false;
if (Security::hasRole(Array(Security::ROLE_ADMIN,Security::ROLE_ENCADRANT,Security::ROLE_SECRETAIRE)) 
        || (isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $adherentedit->getIdAdherent())) {
    $lbAutoriseAdministrationDesDroits = true;
}
$lbEstAdherentConnecté = (isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $adherentedit->getIdAdherent()) ;
$AdminDisabled = "";
if (!Security::hasRole(Array(Security::ROLE_ADMIN)) || $lbEstAdherentConnecté) {
    $AdminDisabled = 'disabled="disabled" ';
}

$envoiMailDisabled = '';
if (isset($_SESSION['idadherent']) && $_SESSION['idadherent'] == $adherentedit->getIdAdherent()) {
    $envoiMailDisabled = 'disabled="disabled" ';
}
$lbAutoriseEcrireTags = false;
if (Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    $lbAutoriseEcrireTags = true;
}
$TAGSdisabled = "";
if (!$lbAutoriseEcrireTags) {
    $TAGSdisabled = 'disabled="disabled" ';
}

$RoRepo = new RoleRepository();
$listeRoles = $RoRepo->getAll();

$TyRegle = new TypeReglementRepository();
$TypesRegle = $TyRegle->getAll();

$Tag = new TagsRepository();
$Tags = $Tag->getAll();

$Pass = new PasseportRepository();
$Passe = $Pass->getAll();

$typeCertificat = new TypeCertificatRepository();
$Certificats = $typeCertificat->getAll();
$edit = false;

if($adherentedit){
    $idAdherent = $adherentedit->getIdAdherent();
} else {
   $idAdherent = NULL;
}

?>

