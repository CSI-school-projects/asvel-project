<?php

declare(strict_types=1);
require_once '../../config/globalConfig.php';

require_once '../../config/appConfig.php';
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

<body>
    <?php include_once 'inc/header.php'; ?>
    <div class="container col-11 py-3">
        <html>

        <style>
            h1 {
                color: #FF0000;
                font-size: 55px;
                font-family: "Helvetica Neue", sans-serif; /* police utilisée par Facebook */
            }
        </style>

        <head>
            <title>Echec Envois</title>
        </head>

        <body>
            <main>
                <h1>Votre commentaire et votre post n'ont pas pu été envoyés !</h1>
                <br>
                <h3>Veuillez réessayer plus tard ... </h3>
            </main>
        </body>

        </html>

    </div>
    <?php include_once 'inc/footer.php' ?>
</body>