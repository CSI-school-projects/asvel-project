<?php
require_once '../../config/globalConfig.php';

require_once 'info-affiliation-prepare.php';
?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>
    <body>
        <?php include_once 'inc/header.php'; ?>

        <form action="" method="POST" id="formAffilier">
            <div class="container col-10 py-4">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0">Affiliation à une fédération ou à un club</h4>
                    </div>
                    <div class="card-body">
                        <?php
                        include_once 'info-affiliation-formfields.php';
// LG 20200504 début
// $valider = '<button type="submit" class="btn btn-primary">Valider</button>';
                        $valider = true ;
                        if ($idAdherent) {
                            $bouton = '<a href="info-adherent.php?id=' . $idAdherent . '"'
                                    . ' style="margin-left: 1%;" class="btn btn-secondary">'
                                    . '<i class="fa fa-backward"></i>'
                                    . ' Retour à l\'adherent'
                                    . '</a>';
                            $footer_listeBoutons = [$bouton];
                        }
// LG 20200504 fin
                        include_once 'inc/footer-valider.php';
                        ?>
                    </div>
                </div>
            </div>
        </form>

    </body>
    <script src ="../js/affilier.js?v=<?=VERSION_APP ?>"></script>
</html>
