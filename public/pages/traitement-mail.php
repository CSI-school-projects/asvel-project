﻿<?php

use App\Entity\Mail;
use App\Repository\MailRepository;
use App\Repository\AdherentRepository;
use Lib\olitsMailer;
use App\Security;

require_once '../../src/Lib/olitsMailer.php';
require_once '../../config/globalConfig.php';

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    echo "Merci de vous authentifier pour accéder à cette fonctionnalité";
    return;
}

$errfile = false;
if (isset($_POST['validation'])) {

// inutilisé    $laDestinataires = filter_input(INPUT_POST, 'destinataire[]', FILTER_SANITIZE_STRING);
    $mail = implode(",", filter_var_array($_POST["destinataires"])); // LG 20200221
    $destinatairesVisibles = filter_input(INPUT_POST, 'destinatairesVisibles', FILTER_SANITIZE_STRING) ?: FALSE;
    $subject = filter_input(INPUT_POST, 'obj', FILTER_SANITIZE_STRING);
    $body = filter_input(INPUT_POST, 'contenu', FILTER_SANITIZE_STRING);
    $mailExpediteur = Lib\olitsMailer::$_infoMail['Sender'] ;
    $mailExpediteurDemandé = filter_input(INPUT_POST, 'mailExpediteur', FILTER_SANITIZE_STRING);
    // L'expéditeur demandé n'est pas l'expéditeur standard
    // Pour que le message ne passe pas dans les spams des destinataires, on doit toujours utilisér comme expéditeur réel celui enregistré dans Lib\olitsMailer::$_infoMail['Sender']
    // L'astuce est alors d'insérer à la fin du corps du message un texte explicatif indiquant qu'il ne faut pas faire "répondre à l'expéditeur
    $body .= "<br><br><B>Attention</B>, pour des raisons de gestion des spams, l'expéditeur de ce message est \"contact@asvelskimontagne.fr\". Mais cette boite mail n'est pas celle de l'expéditeur du message : si vous répondez directement à ce mail, vos messages arriveront au secrétariat de l'ASVEL ski-montagne."
            . "<br>Si vous utilisez la fonction \"répondre à tous\", merci d'enlever l'adresse \"contact@asvelskimontagne.fr\" de la liste des destinataires de votre message." ;
    if ($mailExpediteurDemandé !== $mailExpediteur) {
        // L'expéditeur a précisé une adresse mail : l'insérer dans le corps du message.
        $body .= "<br>Si vous souhaitez répondre uniquement à l'expéditeur, merci d'utiliser <A href=\"mailto:" . $mailExpediteurDemandé . "?subject=Re:" . $subject . "\">cette adresse mail</A>" ;
    }

    $expediteur = $_SESSION['idadherent'];

    $dataMail = array(
        'sujetMail' => $subject,
        'contenuMail' => $body,
        'idadherentexpediteur' => $expediteur,
        'dateMail' => Date('Y-m-d H:i:s'),
        'mailExpediteur' => $mailExpediteur,
        'destinatairesVisibles' => $destinatairesVisibles
    );

    $res = olitsMailer::EnvoyerMail($mail, $subject, $body, $mailExpediteur, $destinatairesVisibles);
    if (is_bool($res)) {
        //l'envoi du mail a reussi
        //enregistrer les mail
        try {
            $Mail = new Mail($dataMail);
            $MailRep = new MailRepository();
            if ($MailRep->vérifier($Mail, $tabErr)) {
                $laDestinataires = [] ;
                $loRepoAdherents = new AdherentRepository() ;
                $laMails = explode(",", $mail) ;
                foreach ($laMails as $lsMail) {
                    $lsMail = explode(":", $lsMail)[0] ;
                    if ($loDestinataire = $loRepoAdherents->getEntityByLogin($lsMail)) {
                        $laDestinataires[] = $loDestinataire->getIdAdherent() ;
                    }   
                }
                $LeMail = $MailRep->sauver($Mail, $laDestinataires);
                echo $LeMail->getIdMail();
            } else {
                echo $tabErr[0];
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    } else {
        // Echec de l'envoi du mail : $res contient l'erreur signalée par olitsMailer
        echo $res;
    }
} else {
    // Pas de champ "Validation"
    echo "Les données transmises sont incorrectes";
}
