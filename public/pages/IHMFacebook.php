<?php

// Lire : LiserMoiAPIFacebook.txt

declare(strict_types=1);
require_once '../../config/globalConfig.php';
require_once '../../config/appConfig.php';

//require_once'../../src/App/Repository/SortieRepository.php';

use App\Repository\SortieRepository;
use App\Repository\ActiviteRepository;
use App\Entity\Activite;
use App\Security;
use Phaln\BDD;

// Création de l'objet SortieRepository
$loRepo = new SortieRepository();


?>

<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<link rel="stylesheet" href="../../public/css/Facebook.css">
<html>
<!-- Envoie à la page App ASVEL -->
<title>Publier sur Facebook</title>

<body>
    <?php include_once 'inc/header.php'; ?>

    <br>

    <h1 style="color: #4267b2; font-weight: bold;text-align:center; height:100px">Publication sur Facebook :</h1>

    <main>
        <form method="POST" action="../pages/traitement-IHMFacebook.php" enctype="multipart/form-data" style="width: 600px;height:520px">
            <input type="hidden" name="csrftoken" value="<?= (isset($_SESSION['token'])) ? $_SESSION['token'] : '' ?>" />

            </div>
            </li>
            <br>
            <!-- On récupère le nom et la date de la sortie grâce au Repository que j'ai crée  -->
            <?php
            // On utilise la méthode du repository nomSortieRéalisé 
            $sortieRéalisé = $loRepo->nomDateSortieRéalisé();

            ?>

            <br><br><br>

            <label for="com" style="color: #4267b2; font-family: Arial, sans-serif; font-weight: bold; margin: 0; font-size: 23px" >Nom sortie + Date de la sortie + Commentaire :</label><br>
            <textarea id="com" name="com" rows="3" cols="70" required="required">
            Canyoning 2023-01-03 : 'Commentaire à publier sur la sortie' 
            </textarea>

            <br><br><br>


            <h4 style="color: #4267b2; font-family: Arial, sans-serif; font-weight: bold; margin: 0;">Image à publier :</h4> <br>

            <br>

            <input type="file" accept="image/png, image/jpeg" id ="image" name="image" required="required">

            <br><br>
            
            <input type="submit" name="Poster">

        </form>
    </main>
    <br>
    <?php 
    // inclusion footer
    include_once 'inc/footer.php' ?>
</body>
