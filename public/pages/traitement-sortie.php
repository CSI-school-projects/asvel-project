<?php

require_once '../../config/globalConfig.php';

use App\Entity\Sortie;
use App\Repository\SortieRepository;
use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_ADHERENT)) {
    header("location: Accessdenied.php");
}

$idSortie = filter_input(INPUT_POST, 'idsortie', FILTER_SANITIZE_STRING);
$nomSortie = filter_input(INPUT_POST, 'nomsortie', FILTER_SANITIZE_STRING);
$DateDebutSortie = filter_input(INPUT_POST, 'datedebutsortie', FILTER_SANITIZE_STRING);
$DateFinSortie = filter_input(INPUT_POST, 'datefinsortie', FILTER_SANITIZE_STRING);
$LieuSortie = filter_input(INPUT_POST, 'lieusortie', FILTER_SANITIZE_STRING);
$DistanceSortie = filter_input(INPUT_POST, 'distancesortie', FILTER_SANITIZE_NUMBER_INT);
$DeniveleSortie = filter_input(INPUT_POST, 'denivellesortie', FILTER_SANITIZE_NUMBER_INT);
$AltitudeMaxSortie = filter_input(INPUT_POST, 'altitudemaxsortie', FILTER_SANITIZE_NUMBER_INT);
$NombreParticipantSortie = filter_input(INPUT_POST, 'nombreparticipantsmaxsortie', FILTER_SANITIZE_NUMBER_INT);
$nbVoitureSortie = filter_input(INPUT_POST, 'nbvoituressortie', FILTER_SANITIZE_NUMBER_INT);
$tarifSortie = filter_input(INPUT_POST, 'tarifsortie', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
$coutAutoroute = filter_input(INPUT_POST, 'coutautoroute', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
$organisateur = filter_input(INPUT_POST, 'organisateur', FILTER_SANITIZE_STRING);
$difficulte = filter_input(INPUT_POST, 'difficulte', FILTER_SANITIZE_NUMBER_INT);
$commentaire = filter_input(INPUT_POST, 'commentairessortie', FILTER_SANITIZE_STRING);
$idRaisonAnnulation = filter_input(INPUT_POST, 'idRaisonAnnulation', FILTER_SANITIZE_NUMBER_INT);
$heuredepart = filter_input(INPUT_POST, 'heuredepart', FILTER_SANITIZE_STRING);
$inscriptionenligne = filter_input(INPUT_POST, 'inscriptionenligne', FILTER_SANITIZE_STRING);
$fraisAnnexe = filter_input(INPUT_POST, 'fraisAnnexe', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
$fraisAnnexeDetails = filter_input(INPUT_POST, 'fraisAnnexeDetails', FILTER_SANITIZE_STRING);

if ($DateFinSortie == "" || NULL) {
    $DateFinSortie = $DateDebutSortie;
}

$datas = array(
    'idsortie' => $idSortie,
    'nomsortie' => $nomSortie,
    'datedebutsortie' => $DateDebutSortie,
    'datefinsortie' => $DateFinSortie ?: Null,
    'lieusortie' => $LieuSortie ?:Null,
    'distancesortie' => $DistanceSortie ?:Null,
    'denivellesortie' => $DeniveleSortie ?:Null,
    'altitudemaxsortie' => $AltitudeMaxSortie ?:Null,
    'nombreparticipantsmaxsortie' => $NombreParticipantSortie ?:Null,
    'nbvoituressortie' => $nbVoitureSortie ?:Null,
    'tarifsortie' => $tarifSortie ?:Null,
    'coutautoroute' => $coutAutoroute ?:Null,
    'idorganisateur' => $organisateur,
    'iddifficulte' => $difficulte ?:Null,
    'activites' => implode(",", filter_var_array($_POST["activites"])),
    'commentairessortie' => $commentaire ?:Null,
    'idRaisonAnnulation' => $idRaisonAnnulation ?: Null,
    'heuredepart' => $heuredepart ?: Null,
    'inscriptionenligne' => $inscriptionenligne ?:FALSE,
    'fraisAnnexe' => $fraisAnnexe ?:Null,
    'fraisAnnexeDetails' => $fraisAnnexeDetails ?:Null,
);
try {
    $Sortie = new Sortie($datas);
    $SortieRep = new SortieRepository();
    if ($SortieRep->vérifier($Sortie, $tabErr)) {
        $LaSortie = $SortieRep->sauver($Sortie);
//        echo 'true';
        echo $LaSortie->getIdSortie() ;
    } else {
        echo $tabErr[0];
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}
//header('Location: info-sortie.php?id=' . $LaSortie->getIdSortie());
