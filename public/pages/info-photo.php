<?php
require_once '../../config/globalConfig.php';
require_once '../../vendor/PDF/autoload.php';

use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
    header('Location: Accessdenied.php');
    return;
}

?>
<!DOCTYPE html>
<link rel="stylesheet" href="../css/datatables.css">

<html>
    <head>
        <title>Upload photo - ASVEL Ski Montagne</title>
        <?php include_once 'inc/head.php'; ?>
    </head>
    <body>
        <header>
            <?php include_once 'inc/header.php'; ?>
        </header>

        <div class="card text-center m-3">
            <h1 class="card-header">Sélection de la photo</h3>
        </div>

        <div class="card-body">
            <div>
                <form action="traitement-photo.php?idSortie=<?php echo $_GET['Activite'];?>" method="POST" enctype="multipart/form-data">
                    <label for="file">Fichier</label>
                    <input type="file" name="file">
                    <button type="submit">Enregistrer</button>
                </form>
            </div>
        </div>

        <div class="card-footer pb-0 pt-3">

        </div>

        <?php include_once 'inc/footer.php'; ?>
    </body>
</html>
