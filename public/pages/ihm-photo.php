<?php
require_once '../../config/globalConfig.php';
require_once '../../vendor/PDF/autoload.php';

use App\Security;

if (!Security::hasRole(Security::ROLE_TOUS_SAUF_VISITEUR)) {
    header('Location: Accessdenied.php');
    return;
}

?>
<!DOCTYPE html>
<link rel="stylesheet" href="../css/datatables.css">
<link rel="stylesheet" href="../css/ihm_photo.css">

<html>
    <head>
        <title>Photos des sorties - ASVEL Ski Montagne</title>
        <?php include_once 'inc/head.php'; ?>
        <style>
            .myButton {
                justify-content: center;
                align-items: center;
                display: flex;
                margin: auto;
                margin-bottom: 50px;
                margin-top: 50px;
                height: 50px;
                width: 120px;
            }
        </style>
    </head>
    <body>
        <header>
            <?php include_once 'inc/header.php'; ?>
        </header>

        <div class="card text-center m-3">
            <h1 class="card-header">Choix du mode de tri des photos</h3>
        </div>

        <div class="card-body">
            <div>
                <a href="ihm-dossier-adherent.php"><button class="myButton">Par adhérents</button></a>
                <a href="ihm-dossier-sortie.php"><button class="myButton">Par sorties</button></a>
            </div>
        </div>

        <div class="card-footer pb-0 pt-3">

        </div>

        <?php include_once 'inc/footer.php'; ?>
    </body>
</html>
