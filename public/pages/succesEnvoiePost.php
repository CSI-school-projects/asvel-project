<?php

declare(strict_types=1);
require_once '../../config/globalConfig.php';

require_once '../../config/appConfig.php';

use App\Security;

if(!Security::hasRole(Security::ENCADRANT))
{
    header('Location: Accessdenied.php');
    return;
}

?>
<!DOCTYPE html>
<?php include_once 'inc/head.php' ?>
<link rel="stylesheet" href="../css/datatables.css">
<html>

<body>
    <?php include_once 'inc/header.php'; ?>
    <div class="container col-11 py-3">
        <html>

        <style>
            h1 {
                color: #3b5998; /* couleur bleue de Facebook */
                font-size: 55px;
                font-family: "Helvetica Neue", sans-serif; /* police utilisée par Facebook */
            }

            .linktoFacebook{
                text-align: center;
                height: 50px;
                font-size: 40px;
                color:#3b5998;
            }
        </style>

        <head>
            <title>Confirmation d'envoi</title>
        </head>

        
        <body>
            <main>
                <h1>Votre commentaire et votre post ont été envoyés avec succès !</h1>
                <p>Nous vous remercions pour votre participation. Vous pouvez visualiser le post en cliquant sur <strong>"Visualiser la Publication Facebook"</strong> ci-dessous :</p>
                <div class ="linktoFacebook">
                    <!-- target="_blank" sert à ouvrir le lien dans une nouvelle fenêtre-->
                <a href ="https://www.facebook.com/profile.php?id=100089135529452" target="_blank" title="Cliquez ici pour visiter le post sur Facebook">>Visualiser la Publication Facebook</a> 
                </div>
            </main>
        </body>
        
        </html>

    </div>
    <?php include_once 'inc/footer.php' ?>
</body>