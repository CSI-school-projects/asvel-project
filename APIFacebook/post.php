
<?php
// Inclusion de la librairie Facebook PHP
require_once '/wamp64/www/APIFacebook/src/Facebook/autoload.php';

// Création d'un objet Facebook\Facebook
$fb = new Facebook\Facebook([
  'app_id' => '746876010294833',
  'app_secret' => 'b66544876a8c615860131236c2ec281d',
  'default_graph_version' => 'v3.3',
]);

// Récupération du chemin vers la photo à publier
$image = 'C:\Users\alexi\Downloads\1. MLD Asvel Actuel.PNG';

// Préparation de la requête à envoyer à l'API Graph
$data = [
  'message' => 'This is a test photo',
  'source' => $fb->fileToUpload($image),
];

// Envoi de la requête à l'API Graph pour publier la photo sur le mur de l'utilisateur
try {
  $response = $fb->post('/me/photos', $data, 'EAAKnR59I7jEBAGt1E3MWZA6XyKmwC03Dwaj8iu9fdhROvRdpasEIQS1icVJBwKZCtNZBhG5xLjGC50FDI47e12WreY7kIVBA28UtCAaAqZB33L2GvcQwJgkDTWyGoNbEPuKqeE4ZBGfdDZAEs9gODq5cRHkbZBjXZBzj6ADkVRCYcgJ3dYYwPPDZCWehAMZCHjcmUZD');
  $graphNode = $response->getGraphNode();
  echo 'Photo ID: ' . $graphNode['id'];
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}


?>