<?php


namespace Lib\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RoleExtension extends AbstractExtension
{

    public function getFunctions()
    {
        return array (
            new TwigFunction('hasRole', array('App\Repository\RoleRepository', 'HasRole')),
        );
    }

    public function getName()
    {
        return 'FuzAppBundle:Role';
    }

}