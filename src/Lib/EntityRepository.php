<?php
namespace Lib;

use Lib\Exceptions\BDDException;
use Lib\Exceptions\NotFoundException;
use Lib\Exceptions\RepositoryException;
use Reflection;
use ReflectionClass;
use ReflectionException;

/**
 * EntityRepository
 */
abstract class EntityRepository {

    /**
     * @param array $class
     * @return array
     */
    private static function convertDataFormat(array $class){
        $mapping = array();
        try {
            $oReflectionClass = new ReflectionClass(static::$classMapped);
            $props = $oReflectionClass->getDefaultProperties();
            foreach($props as $prop => $null){
                foreach($class as $k => $v){
                    if(strtolower($k) == strtolower($prop)){
                        $mapping[$prop] = $v;
                        break;
                    }
                }
            }
        } catch (ReflectionException $e) {}
        return $mapping;
    }

    /**
     * @param $result
     * @return array
     * @throws ReflectionException
     */
    protected static function fetch($result, $obj=true){
        $bdd = isset(static::$classBDD)?static::$classBDD:BDD::class;
        $resultSet = array();
        $cla = $bdd::getType();
        switch ($cla) {
            case 'mysqli':
                while($row = $result->fetch_assoc())
                {
                    $resultSet[] = $obj ? new static::$classMapped(self::convertDataFormat($row)) : $row;
                }
                break;
            case 'PDO':
                while($row = $result->fetch(\PDO::FETCH_ASSOC))
                {
                    $resultSet[] = $obj ? new static::$classMapped(self::convertDataFormat($row)) : $row;
                }
                break;
            default:
                ExceptionsManager::addException(new RepositoryException('Une erreur est survenue dans EntityRepository::fetch()'));
                break;
        }
        return $resultSet;
    }

    /**
     * Récupère tous les enregistrements de la table.
     * @return array|null un tableau d'objet de classe classMapped dérivant Entite
     * @throws ReflectionException
     */
    public static function getAll()
    {
        $bdd = isset(static::$classBDD)?static::$classBDD:BDD::class; 
        
        $query = 'SELECT * FROM '.static::$table;
        
        $rqtResult = $bdd::query($query);
        
        if($rqtResult)
        {
            return self::fetch($rqtResult);
        }else{
            ExceptionsManager::addException(new BDDException($bdd->getDB()->error));
        }
        return false;
    }

    /**
     * Récupère l'enregistrement n° $id dans la table
     * @param int $id la valeur de la pk à récupérer
     * @return Entite un objet de classe classMapped dérivant Entite
     * @throws ReflectionException
     */
    public static function getEntityId($id)
    {
        if(!isset($id)){
            ExceptionsManager::addException(new RepositoryException('Missing parameters in the repository '.__CLASS__.' for the function '.__FUNCTION__));
            return new static::$classMapped();
        }
        $bdd = isset(static::$classBDD)?static::$classBDD:BDD::class;
        $resultSet = NULL;
        $cla = $bdd::getType();
        switch ($cla) {
            case 'mysqli':
                $query = 'SELECT * FROM '.static::$table .
                        ' WHERE '.static::$idFieldName.' = ?';
                $reqPrep = $bdd::getDB()->prepare($query);
                $reqPrep->bind_param('i',$id);
                $rqtResult = $reqPrep->execute();
                if($rqtResult != FALSE)
                {
                    $res = $reqPrep->get_result();
                    $row = $res->fetch_assoc();
                    if($row)
                    $resultSet = new static::$classMapped($row);
                }else{
                    ExceptionsManager::addException(new BDDException("Une erreur est survenue lors de l'exécution de cette requete : \n ".$reqPrep->queryString));
                }
                break;
            case 'PDO':
                $query = 'SELECT * FROM '.static::$table .
                        ' WHERE '.static::$idFieldName.' = '.$id;
                $rqtResult = BDD::query($query);
                while($row = $rqtResult->fetch(\PDO::FETCH_ASSOC))
                {
                    $resultSet = new static::$classMapped(self::convertDataFormat($row));
                }
                break;
            default:
                ExceptionsManager::addException(new RepositoryException('Une erreur est survenue dans EntityRepository::getEntityId()'));
                break;
        }
        return $resultSet;
    }

    /**
     * @param Entite $entity
     * @param $type
     * @return bool
     * @throws ReflectionException
     */
    public static function sauver(Entite $entity, $type)
    {
        if(!isset($type) || !isset($entity)){
            ExceptionsManager::addException(new RepositoryException('Missing parameters in the repository '.__CLASS__.' for the function '.__FUNCTION__));
            return false;
        }
        $bdd = isset(static::$classBDD)?static::$classBDD:BDD::class;
        $idField = 'get'.ucfirst(static::$idFieldName);
        if($type=="update" && $entity->$idField() == null){
            ExceptionsManager::addException(new RepositoryException('Invalid id entity in the repository '.__CLASS__.' for the function '.__FUNCTION__.' for update action'));
            return false;
        }
        $build = $bdd::createBuilder($type);
        try {
            $reflection = new ReflectionClass($entity);
            $build->setTable($reflection->getShortName());
            $properties = $reflection->getDefaultProperties();

            foreach ($properties as $property => $null){
                $getter = 'get'.ucfirst($property);
                $val =  $entity->$getter();
                if($property == static::$idFieldName){
                    $build->setId(static::$idFieldName, $val);
                    continue;
                }
                if($val != null){
                    $build->addValue($property, $val);
                }
            }
            return $build->exec();
        } catch (ReflectionException $e) {
            ExceptionsManager::addException(new NotFoundException("La classe '$e' n'est pas valide ou introuvable."));
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     * @throws ReflectionException
     */
    public static function delete($id) {
        if(!isset($id)){
            ExceptionsManager::addException(new RepositoryException('Missing parameters in the repository '.__CLASS__.' for the function '.__FUNCTION__));
            return false;
        }
        $bdd = isset(static::$classBDD)?static::$classBDD:BDD::class;
        return $bdd::query("DELETE FROM ".static::$table." WHERE ".static::$idFieldName."=$id");
    }
    
    
    
}
