<?php

namespace Lib;

require_once ("../../vendor/PHPMailer-master/src/PHPMailer.php");
require_once ("../../vendor/PHPMailer-master/src/Exception.php");
require_once ("../../vendor/PHPMailer-master/src/SMTP.php");

use PHPMailer\PHPMailer\PHPMailer;

final class olitsMailer {

	public static $_infoMail;	  // Rempli par appConfig.php

	/** Envoyer un mail
	 * $to		: liste d'adresses sous forme de liste avec séparateur virgule : <mail1>[:<nom1>][[,<mail2>[:<nom2>][...]]
	 * $subject
	 * $body
	 */
	public static function EnvoyerMail($to, $subject, $body, $sender="", $destinatairesVisibles=true) {

		$mailer = new PHPMailer();  // Cree un nouvel objet PHPMailer
		$mailer->IsSMTP(); // active SMTP
		$mailer->SMTPDebug = self::$_infoMail["SMTPDebug"];  // debogage: 1 = Erreurs et messages, 2 = messages seulement
		$mailer->SMTPAuth = self::$_infoMail["SMTPAuth"];  // Authentification SMTP active
		$mailer->SMTPSecure = self::$_infoMail["SMTPSecure"];

		$mailer->Host = self::$_infoMail["Host"];
		$mailer->Port = self::$_infoMail["Port"];
		$mailer->Username = self::$_infoMail["Username"];
		$mailer->Password = self::$_infoMail["Password"];

		$mailer->isHTML(true);			// Paramétrer le format des emails en HTML ou non
		$mailer->CharSet = 'UTF-8';		// Encodage du message (accents, ...)
		$sender = $sender?$sender:self::$_infoMail["Sender"] ;
		$mailer->SetFrom($sender);
		$mailer->Subject = $subject;
		$mailer->Body = str_replace(chr(13), "<br>", $body);

		$laTo = explode(",", $to) ;
		foreach($laTo as $lsTo) {
			if (strpos($lsTo, ":")) {
				// Un nom de destinataire a été fourni en plus de l'adresse
				if ($destinatairesVisibles) {
					$mailer->addAddress(explode(":", $lsTo)[0], explode(":", $lsTo)[1]);
				} else {
					$mailer->AddBCC(explode(":", $lsTo)[0], explode(":", $lsTo)[1]);
				}
			} else {
				// Seule l'adresse du destinataire a été fournie
				if ($destinatairesVisibles) {
					$mailer->addAddress($lsTo);
				} else {
					$mailer->AddBCC($lsTo);
				}
			}
		}
// LG 20220513 début
		// if (!$mailer->Send()) {
		// 	return 'Mail error: ' . $mailer->ErrorInfo;
		// } else {
		// 	return true;
		// }
		ob_start() ;
		$lsMailError = "" ;
		try {
			$lbSent = $mailer->Send() ;
			if (!$lbSent) {
				$lsMailError = "Erreur du serveur de mail : " . $mailer->ErrorInfo ;
			}
		} catch (\Exception $e) {
			$lsMailError = "Erreur dans l'exécution de l'envoi du mail : " . ob_get_contents() ;
			$lbSent = false ;
		}
		ob_end_clean() ;
		
//		ob_start() ;
//		var_dump($mailer->getAllRecipientAddresses()) ;		
//		$lsLstTo2 = ob_get_contents() ;
//		ob_end_clean() ;		
		
		$lsInfosMail = "To : $to
From: $sender
Subject: $subject\r\n" ;
		if (!$lbSent) {
// LG 20220513 début            
			$lsLog = date('Y-m-d H:i:s') . " : "  . $lsMailError . "\n" ;
			$lsFileName = "../../logs/EnvoisMailKO.log" ;
			file_put_contents($lsFileName, $lsLog, FILE_APPEND) ;
			echo $lsMailError;
			return false ;
// LG 20220513 fin            
		} else {
// LG 20220513 début            
//			$lsLog = date('Y-m-d H:i:s') . " : "  . $lsInfosMail . "\n" ;
//			$lsFileName = "../../logs/EnvoisMailOK.log" ;
//			file_put_contents($lsFileName, $lsLog, FILE_APPEND) ;
// LG 20220513 fin            
			return true;
		}
// LG 20220513 fin
	}

}
