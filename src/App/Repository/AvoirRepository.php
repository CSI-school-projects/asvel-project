<?php

namespace App\Repository;

class AvoirRepository extends \Phaln\AbstractRepository {
    protected $table = 'Avoir';     // le nom de la table manipulée
    protected $idFieldName = 'id';  // le nom du champ clé primaire. id par défaut.
	protected $classMapped = 'App\Entity\Avoir';  // le nom de la classe mappée
	protected $notFieldProps = [];  // tableau des propriétés qui ne sont pas des champs de la table sous-jacente
}