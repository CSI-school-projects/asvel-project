<?php
namespace App\Repository;
use \App\Lib\Functions;

class TypeLicenseRepository extends \Phaln\AbstractRepository
{
    protected $table = 'TypeLicense';					// le nom de la table manipulée
    protected $classMapped = 'App\Entity\TypeLicense';	// le nom de la classe mappée
    protected $idFieldName = 'idLicense';			// le nom du champ clé primaire. id par défaut.
    protected $notFieldProps = [];	
    protected $uniqueProps = [["nomTypeLicense"]];
   
    public function getEntityByName($nomTypeLicense, $pbAutoriseCreeNouveau = false) {
        $SQL = 'SELECT * FROM ' . $this->table
                . ' WHERE normalise(nomTypeLicense) = :nom' ;
		$reqPrep = $this->db->prepare($SQL);
		$reqPrep->bindValue(':nom', Functions::normalise($nomTypeLicense));
		$entity = $this->getEntityByReqPrep($reqPrep);
		if (!$entity && $pbAutoriseCreeNouveau) {
			// Il n'y a pas encore d'entité à ce nom, et on autorise la création
			// Créer l'entité maintenant
			$entity = new $this->classMapped(["nomTypeLicense"=>$nomTypeLicense]) ;
// var_dump($entity) ;
			$entity = $this->sauver($entity) ;
		}
        return $entity;
     }

}
