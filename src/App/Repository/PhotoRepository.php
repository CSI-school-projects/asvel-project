<?php

namespace App\Repository;

use App\Entity\Photo;
use Phaln\BDD;
use \PDO;

class PhotoRepository extends \Phaln\AbstractRepository {
    protected $table = 'Photo';     // le nom de la table manipulée
    protected $classMapped = 'App\Entity\Photo'; // le nom de la classe mappée
    protected $idFieldName = 'idPhoto';   // le nom du champ clé primaire. id par défaut.

    public function getPhotoByAdherentId($idAdherent) {
        $query = "SELECT idphoto, cheminphoto, idadherent FROM Photo WHERE idadherent = $idAdherent ORDER BY idphoto;";
        $req = $this->db->prepare($query);
        $req->execute();
        $datas = $req->fetchAll(PDO::FETCH_ASSOC);

        return $datas;
    }

    public function getPhotoBySortieId($idSortie) {
        $query = "SELECT idphoto, cheminphoto, idsortie FROM Photo WHERE idsortie = $idSortie ORDER BY idphoto;";
        $req = $this->db->prepare($query);
        $req->execute();
        $datas = $req->fetchAll(PDO::FETCH_ASSOC);

        return $datas;
    }

    public function insertPhoto($heure, $cheminPhoto, $idAdherent, $idSortie) {
        $query = 'INSERT INTO Photo(dateheurephoto, cheminphoto, idadherent, idsortie) VALUES (?, ?, ?, ?);';
        $req = $this->db->prepare($query);
        $req->execute([$heure, $cheminPhoto, $idAdherent, $idSortie]);
    }
}
