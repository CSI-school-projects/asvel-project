<?php
namespace App\Repository;

class PasseportRepository extends \Phaln\AbstractRepository
{
    protected $table = 'passeport';
    protected $classMapped = \App\Entity\Passeport::class;
//    protected $classMapped = 'App\Entity\Passeport';
    protected $idFieldName = 'idPasseport';

    public function getPasseportByAdherent($id){
		$query = "SELECT passeport.* from obtenir, passeport where obtenir.idpasseport = passeport.idpasseport AND idadherent=:id" ;
		$reqPrep = $this->db->prepare($query);
		$reqPrep->bindValue(':id', $id);
		return $this->getAllByReqPrep($reqPrep);
    }
    public function sauver(\Phaln\AbstractEntity $entity)
    {
        // TODO: Implement sauver() method.
    }
}
