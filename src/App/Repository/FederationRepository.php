<?php
namespace App\Repository;
use \App\Lib\Functions;

class FederationRepository extends \Phaln\AbstractRepository
{
	protected $table = 'federation';					// le nom de la table manipulée
	protected $classMapped = 'App\Entity\Federation';	// le nom de la classe mappée
	protected $idFieldName = 'idfederation';			// le nom du champ clé primaire. id par défaut.
	protected $notFieldProps = [];	
    
	public function getDistinctByAdherent($idAdherent) {
        $SQL = 'SELECT DISTINCT federation.* FROM affilier,federation'
                . ' WHERE idadherent = :idAdherent AND affilier.idfederation = federation.idfederation' ;
		$reqPrep = $this->db->prepare($SQL);
		$reqPrep->bindValue(':idAdherent', $idAdherent);
		$resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
     }
   
	public function getEntityByName($nomFederation) {
        $SQL = 'SELECT federation.* FROM federation'
                . ' WHERE normalise(nomFederation) = :nom' ;
		$reqPrep = $this->db->prepare($SQL);
		$reqPrep->bindValue(':nom', Functions::normalise($nomFederation));
		$entity = $this->getEntityByReqPrep($reqPrep);
        return $entity;
     }
}
