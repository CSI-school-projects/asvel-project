<?php

namespace App\Repository;

use App\Entity\Materiel;
// LG 20200208 déac use Phaln\BDD;

class MaterielRepository extends \Phaln\AbstractRepository
{
    /*
    public function __construct() {

        $this->table = 'activite';
        $this->db = BDD::getConnexion();
        if(!$this->db)
            throw new RepositoryException('Pb db dans PersonneRepository::_construct()');

    }
*/
    protected $table = 'materiel';                    // le nom de la table manipulée
    protected $classMapped = 'App\Entity\Materiel';    // le nom de la classe mappée
    protected $idFieldName = 'idmateriel';            // le nom du champ clé primaire. id par défaut.

    public function createEntite($materiel)
    {
        $bindParam = array(
            'tailleMateriel' => $materiel['tailleMateriel'],
            'pointureMateriel' => $materiel['pointureMateriel'],
            'modeleMateriel' => $materiel['modeleMateriel'],
            'latitude1Materiel' => 234,
            'latitude2Materiel' => 423,
            'longitude1Materiel' => 567,
            'longitude2Materiel' => 987,
            'dateachatMateriel' => $materiel['dateachatMateriel'],
            'commentaireMateriel' => $materiel['commentaireMateriel'],
            'paysCarteMateriel' => 'France',
            'idAdherent' => $materiel['idAdherent'],
            'idTypeMateriel' => $materiel['idTypeMateriel'],
            'idTypeMiseADispo' => $materiel['idTypeMiseADispo'],
        );
        var_dump($bindParam);

        // Nouvelle entité
        $query = 'INSERT INTO '. $this->table
            . '(tailleMateriel, pointureMateriel, modeleMateriel, latitude1Materiel, latitude2Materiel, '
            . 'longitude1Materiel, longitude2Materiel, dateachatMateriel, commentaireMateriel, paysCarteMateriel, '
            . 'idAdherent, idTypeMateriel, idTypeMiseADispo) '
            . 'values (:tailleMateriel, :pointureMateriel, :modeleMateriel, :latitude1Materiel, :latitude2Materiel, '
            . ':longitude1Materiel, :longitude2Materiel, :dateachatMateriel, :commentaireMateriel, :paysCarteMateriel, '
            . ':idAdherent, :idTypeMateriel, :idTypeMiseADispo)';

        $reqPrep = $this->db->prepare($query);
        $reqPrep->bindParam(':tailleMateriel', $bindParam['tailleMateriel'], \PDO::PARAM_INT);
        $reqPrep->bindParam(':pointureMateriel', $bindParam['pointureMateriel'], \PDO::PARAM_INT);
        $reqPrep->bindParam(':modeleMateriel', $bindParam['modeleMateriel'], \PDO::PARAM_STR);
        $reqPrep->bindParam(':latitude1Materiel', $bindParam['latitude1Materiel'], \PDO::PARAM_INT);
        $reqPrep->bindParam(':latitude2Materiel', $bindParam['latitude2Materiel'], \PDO::PARAM_INT);
        $reqPrep->bindParam(':longitude1Materiel', $bindParam['longitude1Materiel'], \PDO::PARAM_INT);
        $reqPrep->bindParam(':longitude2Materiel', $bindParam['longitude2Materiel'], \PDO::PARAM_INT);
        $reqPrep->bindParam(':dateachatMateriel', $bindParam['dateachatMateriel'], \PDO::PARAM_STR);
        $reqPrep->bindParam(':commentaireMateriel', $bindParam['commentaireMateriel'], \PDO::PARAM_STR);
        $reqPrep->bindParam(':paysCarteMateriel', $bindParam['paysCarteMateriel'], \PDO::PARAM_STR);
        $reqPrep->bindParam(':idAdherent', $bindParam['idAdherent'], \PDO::PARAM_INT);
        $reqPrep->bindParam(':idTypeMateriel', $bindParam['idTypeMateriel'], \PDO::PARAM_INT);
        $reqPrep->bindParam(':idTypeMiseADispo', $bindParam['idTypeMiseADispo'], \PDO::PARAM_INT);
        $reqPrep->execute();
    }

    public function getEntitesById($id)
    {
        return $this->getEntityById($id);
    }

    public function getAll()
    {
        $query = 'SELECT * FROM ' . $this->table . ' Order by ' . $this->idFieldName . ';';
        $reqPrep = $this->db->prepare($query);
        $Result = $this->getAllByReqPrep($reqPrep);
        return $Result;
    }

    public function getByTypeMateriel($type)
    {
        $query = 'SELECT * FROM ' . $this->table . ' where idtypemateriel = ' . $type . ' Order by ' . $this->idFieldName . ' ;';
        $reqPrep = $this->db->prepare($query);
        $Result = $this->getAllByReqPrep($reqPrep);
        return $Result;
    }
}
