<?php
namespace App\Repository;

class RoleRepository extends \Phaln\AbstractRepository {

	protected $table = 'role';
	protected $classMapped = 'App\Entity\Role';
	protected $idFieldName = 'idRole';
	
    public function __construct() {

        parent::__construct();
    }

/*
    public function getRolesByUser($id) {
        $sth = $this->db->prepare('SELECT role.* FROM etre,role'
                . ' WHERE idadherent = :idadherent AND etre.idrole =role.idrole');
//        var_dump($sth);
        $resultat = $sth->execute(array(':idadherent' => $id));
        if ($resultat) {
            while ($row = $sth->fetch(\PDO::FETCH_ASSOC)) {
                $resultSet[] = new Role($row);
            }
        }
        return $resultSet;
     }
 */
    public function getRolesByUser($idAdherent) {
        $SQL = 'SELECT role.* FROM etre,role'
                . ' WHERE idadherent = :idAdherent AND etre.idrole = role.idrole' ;
		$reqPrep = $this->db->prepare($SQL);
		$reqPrep->bindValue(':idAdherent', $idAdherent);
		$resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
     }

    public function sauverRolesAdherent(\App\Entity\Adherent $entity) {
		$laRoles = $entity->getRoles(true) ;
		if ($laRoles == Null) {
			return true ;
		}

		$lbOK = true ;
		try {
			// Démarrer une transaction car notre action demande plusieurs requêtes. Si l'une échoue, tout doit être annulé
			$this->db->beginTransaction();
			
			// Sauver chacun des rôles de la liste et construire la liste des rôles présents
			$lsLstRoles = "";
// var_dump($laRoles) ;
			foreach ($laRoles as $role){
				// Ajouter ce rôle à la liste
				if ($lsLstRoles) $lsLstRoles .= "," ;
				$lsLstRoles .= $role->getIdRole() ;

				// Enregistrer ce rôle
				$query = "Select Count(*) as iCnt From etre Where idAdherent = :idAdherent And idRole = :idRole ;" ;
				dump_var($query, DUMP, "Requête de test pour savoir s'il faut insérer") ;
				$reqPrep = $this->db->prepare($query);
				$reqPrep->bindValue(':idAdherent', $entity->getIdAdherent());
				$reqPrep->bindValue(':idRole', $role->getIdRole());
				if ($reqPrep->execute()) {
					$nouveau = ($reqPrep->fetch(\PDO::FETCH_NUM)[0] == 0) ;
					if ($nouveau) {
						// Il faut insérer ce nouveau rôle pour cet adhérent
						$query = "Insert Into etre (idAdherent, idRole) Values (:idAdherent, :idRole) ;" ;
						dump_var($query, DUMP, "Requête d'insertion") ;
						$reqPrep = $this->db->prepare($query);
						$reqPrep->bindValue(':idAdherent', $entity->getIdAdherent());
						$reqPrep->bindValue(':idRole', $role->getIdRole());
						$reqPrep->execute();
					}
				} else {
					dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
					throw new \Phaln\Exceptions\RepositoryException('Pb db dans RoleRepository::sauverRolesAdherent(): ' . $reqPrep->errorInfo()[2]);
				}
			}

			// Effacer tous les rôles de l'adhérent qui ne sont pas dans la liste
			$query = "Delete From etre Where idAdherent = :idAdherent" ;
			if ($lsLstRoles) {
				// Cet adhérent a des rôles
				$query .= " And Not idRole In (" . $lsLstRoles . ")" ;				
			}
			$query .= ";" ;
			$reqPrep = $this->db->prepare($query);
			$reqPrep->bindValue(':idAdherent', $entity->getIdAdherent());
			if (!$reqPrep->execute()) {
				dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
				throw new \Phaln\Exceptions\RepositoryException('Pb db dans RoleRepository::sauverRolesAdherent(): ' . $reqPrep->errorInfo()[2]);
			}

			// Terminer la transaction en validant
			$this->db->commit();
			
		} catch (\Exception $e) {
			// Terminer la transaction en annulant
			$this->db->rollBack();
			throw new \Phaln\Exceptions\RepositoryException('Pb db dans RoleRepository::sauverRolesAdherent(): ' . $e->getMessage());
		}
		
		return $lbOK ;
    }

    public function sauver(\Phaln\AbstractEntity $entity) {
        // TODO: Implement sauver() method.
    }

}
