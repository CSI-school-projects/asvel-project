<?php

namespace App\Repository;

use App\Entity\Sortie;
use PDO;
use Phaln\BDD;

class SortieRepository extends \Phaln\AbstractRepository
{

    protected $table = 'Sortie';  // le nom de la table manipulée
    protected $idFieldName = 'idSortie'; // le nom du champ clé primaire. id par défaut.
    protected $classMapped = 'App\Entity\Sortie';  // le nom de la classe mappée
    protected $notFieldProps = ["participations", "activites", "nomsActivites"];  // tableau des propriétés qui ne sont pas des champs de la table sous-jacente
    protected $mandatoryProps = ["nomsortie", "idorganisateur"];
    // LG 20201206 déac : l'unicité sur date, heure, lieu, empêche la création lors de l'alaboration du programme, quand on ne connait pas le lieu ni l'heure     protected $uniqueProps = [["nomsortie"], ["dateDebutSortie", "heuredepart", "lieuSortie"]];
    protected $uniqueProps = [["nomsortie"]];

    /**
     * Renvoie la liste des entités correspondant aux sorties, passées ou à venir
     * @param $sortiePassees boolean        : $sortiePassees true pour les sorties passées
     * @param $typeActivite                 : le type d'activité à filtrer
     */
    // LG 20211106 début
    //    public function getAllFiltré($sortiePassees, $typeActivite) {
    //        if ($sortiePassees) {
    //            $query = 'SELECT sortie.* FROM sortie, concerner WHERE sortie.idsortie=concerner.idsortie AND concerner.idactivite = ' . $typeActivite . ' AND datedebutsortie < NOW();';
    //        } else {
    //            $query = 'SELECT sortie.* FROM sortie, concerner WHERE sortie.idsortie=concerner.idsortie AND concerner.idactivite = ' . $typeActivite . ' AND datedebutsortie >= NOW();';
    //        }
    public function getAllFiltré($piFiltreDate, $typeActivite = null, $piAdherent = null)
    {
        $lsWhere = "";
        if ($piFiltreDate == FILTRESORTIE_TOUTESDATES) {
        } else if ($piFiltreDate == FILTRESORTIE_DATESPASSEES) {
            $lsWhere .= " AND datedebutsortie < NOW()";
        } else if ($piFiltreDate == FILTRESORTIE_DATESAVENIR) {
            $lsWhere .= " AND datedebutsortie > NOW()";
        }
        if ($piAdherent) {
            $lsWhere .= " AND sortie.idSortie In (Select idSortie From Participer Where idAdherent = " . $piAdherent . ")";
        }
        $query = 'SELECT sortie.* FROM sortie, concerner'
            . ' WHERE sortie.idsortie=concerner.idsortie'
            . ' AND concerner.idactivite = ' . ($typeActivite ? $typeActivite : "concerner.idactivite")
            . $lsWhere
            . ' ;';
        // LG 20211106 fin
        $reqPrep = $this->db->prepare($query);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }

    // Renvoie toutes les sorties
    // CP 20230601
    public function getAllSortie() {
        $query = "SELECT idsortie, nomsortie from Sortie ORDER BY idsortie;";
        $req = $this->db->prepare($query);
        $req->execute();
        $datas = $req->fetchAll(PDO::FETCH_ASSOC);

        return $datas;
    }

    // Renvoie toutes les sorties par Id
    public function getSortieById($idsortie) {
        $query = "SELECT idsortie, nomsortie from Sortie WHERE idsortie = $idsortie ORDER BY idsortie;";
        $req = $this->db->prepare($query);
        $req->execute();
        $data = $req->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }
    
    /**
     * Renvoie la liste des entités correspondant aux sorties, passées ou à venir
     * @param int $piNb       : le nombre de sorties à venir (dft = 5)
     */
    public function getProchainesSorties($piNb = 4)
    {
        //        $query = 'SELECT * FROM Sortie Where dateDebutSortie > current_date ORDER BY datedebutsortie LIMIT ' . $piNb ;
        $query = 'SELECT * FROM Sortie Where dateDebutSortie > current_date And Coalesce(idRaisonAnnulation, 0) = 0 ORDER BY datedebutsortie LIMIT ' . $piNb;
        $reqPrep = $this->db->prepare($query);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }

    /**
     * Renvoie la liste des sorties pour l'édhérent fourni
     * @param int $piIdAdherent     : l'adhérent pour lequel on veut avoir les sorties
     */
    public function getAllByAdherent($piIdAdherent)
    {
        $query = 'SELECT S.* '
            . ' FROM Sortie S'
            . ' JOIN Participer P On P.idSortie = S.idSortie '
            . ' WHERE P.idAdherent = ' . $piIdAdherent
            . ' ORDER BY datedebutsortie';
        $reqPrep = $this->db->prepare($query);
        $resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }

    /**
     * Ajoute à un tableau de données correspondant à une entité les éléments provenant d"entités liées
     * @param type $row tableau associatif des propriétés de l'entité
     * @param $aSubEntities : tableau des sous-entités à charger
     * @return tableau associatif des propriétés de l'entité, plus les données des entités liées demandées
     */
    public function addSubEntities($row, $aSubEntities = null)
    {
        if (in_array("participations", $aSubEntities)) {
            // On a demandé la liste des participations à cette séance
            $Repo = new ParticiperRepository();
            $participations = $Repo->getParticipationsBySortie($row['idsortie']);
            $Table = array("participations" => $participations);
            $row = array_merge($row, $Table);
        }
        return $row;
    }

    public function sauver(\Phaln\AbstractEntity $entity)
    {
        $entity = parent::sauver($entity);
        if ($entity != Null) {
            // L'enregistrement de l'entité elle-même a réussi : il faut maintenant enregistrer les listes
            if ($entity->getActivites(true) != Null) {
                // On a chargé le propriété "rôles" : il faut enregistrer chaque rôle
                $repo = new ActiviteRepository();
                $repo->sauverActivitesSortie($entity);
            }
        }
        return $entity;
    }

    public function nomDateSortieRéalisé()
    {
        // Création de la requête sql
        $query = 'SELECT nomsortie, datedebutsortie
        FROM sortie 
        where datefinsortie < CURRENT_DATE 
        ORDER BY 2 DESC;';
        // préparation de la requête préparé
        $stmt = $this->db->prepare($query);
        // exécution de la requête préparé
        $stmt->execute();

        // Création de liste déroulante
        echo '<label style="color: #4267b2; font-family: Arial, sans-serif;font-size: 23px; font-weight: bold; margin: 0;"for="nomDate">Nom et date de sortie réalisé:</label>';
        echo '<br>';
        echo '<select style="font-family: Arial, sans-serif; font-weight: bold;" name="nomDate" id="nomDate" required>';
        // Choix dans la liste déroulante du nom et de la date de la sortie
        // Parcourir toutes les sorties passées avec un while et récupérer leur noms et la date de la sortie uniqement. 
        while ($row = $stmt->fetch()) {
            echo '<option value="' . ' ' . '"required>' . $row['nomsortie'] . ' '.$row['datedebutsortie']. '</option>';
            }
        echo '</select>';

        // On retourne le résultat de la requête préparé.
        return $stmt;
        
    }

    // LG 20200208 fin
}