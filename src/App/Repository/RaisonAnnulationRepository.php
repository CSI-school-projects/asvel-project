<?php

namespace App\Repository;

class RaisonAnnulationRepository extends \Phaln\AbstractRepository {

    protected $table = 'RaisonAnnulation';     // le nom de la table manipulée
    protected $classMapped = 'App\Entity\RaisonAnnulation'; // le nom de la classe mappée
    protected $idFieldName = 'idAnnulation';   // le nom du champ clé primaire. id par défaut.
    protected $notFieldProps = [];
    protected $mandatoryProps = ["libelleAnnulation"];
    protected $uniqueProps = [["libelleAnnulation"]];
    public function getLibelleRaisonBySortie($id) {
        $resultSet = NULL;

        $query = 'SELECT libelleannulation FROM raisonannulation,sortie WHERE idsortie = :idFieldName AND sortie.idraisonannulation = raisonannulation.idannulation';
        $reqPrep = $this->db->prepare($query);
        $reqPrep->bindValue(':idFieldName', $id);
        $reqPrep->execute();
        while ($row = $reqPrep->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet = new \App\Entity\RaisonAnnulation($row);
        }
        return $resultSet;
    }

}
