<?php

namespace App\Repository;

use App\Entity\TypeCertificat;
use Phaln\BDD;
use \PDO;

class TypeCertificatRepository extends \Phaln\AbstractRepository {

//     protected $db = NULL;								// la base de donnée (PDO ou mysqli)
    protected $table = 'typecertificat';						// le nom de la table manipulée
    protected $idFieldName = 'idTypeCertificat';				// le nom du champ clé primaire. id par défaut.
    protected $classMapped = 'App\Entity\TypeCertificat';		// le nom de la classe mappée
}