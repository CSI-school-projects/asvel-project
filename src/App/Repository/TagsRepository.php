<?php
namespace App\Repository;
use \App\Lib\Functions;

class tagsRepository extends \Phaln\AbstractRepository
{
	protected $table = 'tag';					// le nom de la table manipulée
	protected $classMapped = 'App\Entity\Tag';	// le nom de la classe mappée
	protected $idFieldName = 'idtag';			// le nom du champ clé primaire. id par défaut.
	protected $notFieldProps = [];	
    protected $uniqueProps = [["NomTag"]];

	
	/**
	 * Surcharge de la méthode par défaut, avec tri par nom.
	 */
	public function getAll() {
		$query = 'SELECT * FROM ' . $this->table . ' ORDER BY NomTag ';
		$reqPrep = $this->db->prepare($query);
		$resultSet = $this->getAllByReqPrep($reqPrep);
		return $resultSet;
	}
	
	
    public function getTagsByUser($idAdherent) {
        $SQL = 'SELECT tag.* FROM avoir,tag'
                . ' WHERE idadherent = :idAdherent AND avoir.idtag = tag.idtag'
				. ' ORDER BY tag.NomTag';
		$reqPrep = $this->db->prepare($SQL);
		$reqPrep->bindValue(':idAdherent', $idAdherent);
		$resultSet = $this->getAllByReqPrep($reqPrep);
        return $resultSet;
    }

    public function sauverTagsAdherent(\App\Entity\Adherent $entity) {
		$laTags = $entity->getTags(true) ;
		if ($laTags == Null) {
			return true ;
		}

		$lbOK = true ;
		try {
			// Démarrer une transaction car notre action demande plusieurs requêtes. Si l'une échoue, tout doit être annulé
			$this->db->beginTransaction();
			
			// Sauver chacun des tags de la liste et construire la liste des tags présents
			$lsLstTags = "";
// var_dump($laTags) ;
			foreach ($laTags as $tag){
				// Ajouter ce tag à la liste
				if ($lsLstTags) $lsLstTags .= "," ;
				$lsLstTags .= $tag->getIdTag() ;

				// Enregistrer ce tag
				$query = "Select Count(*) as iCnt From avoir Where idAdherent = :idAdherent And idTag = :idTag ;" ;
				dump_var($query, DUMP, "Requête de test pour savoir s'il faut insérer") ;
				$reqPrep = $this->db->prepare($query);
				$reqPrep->bindValue(':idAdherent', $entity->getIdAdherent());
				$reqPrep->bindValue(':idTag', $tag->getIdTag());
				if ($reqPrep->execute()) {
					$nouveau = ($reqPrep->fetch(\PDO::FETCH_NUM)[0] == 0) ;
					if ($nouveau) {
						// Il faut insérer ce nouveau tag pour cet adhérent
						$query = "Insert Into avoir (idAdherent, idTag) Values (:idAdherent, :idTag) ;" ;
						dump_var($query, DUMP, "Requête d'insertion") ;
						$reqPrep = $this->db->prepare($query);
						$reqPrep->bindValue(':idAdherent', $entity->getIdAdherent());
						$reqPrep->bindValue(':idTag', $tag->getIdTag());
						$reqPrep->execute();
					}
				} else {
					dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
					throw new \Phaln\Exceptions\RepositoryException('Pb db dans TagRepository::sauverTagsAdherent(): ' . $reqPrep->errorInfo()[2]);
				}
			}

			// Effacer tous les tags de l'adhérent qui ne sont pas dans la liste
			$query = "Delete From avoir Where idAdherent = :idAdherent" ;
			if ($lsLstTags) {
				// Cet adhérent a des tags
				$query .= " And Not idTag In (" . $lsLstTags . ")" ;				
			}
			$query .= ";" ;
			$reqPrep = $this->db->prepare($query);
			$reqPrep->bindValue(':idAdherent', $entity->getIdAdherent());
			if (!$reqPrep->execute()) {
				dump_var($reqPrep->errorInfo(), DUMP, 'PDOStatement::errorInfo():');
				throw new \Phaln\Exceptions\RepositoryException('Pb db dans TagRepository::sauverTagsAdherent(): ' . $reqPrep->errorInfo()[2]);
			}

			// Terminer la transaction en validant
			$this->db->commit();
			
		} catch (\Exception $e) {
			// Terminer la transaction en annulant
			$this->db->rollBack();
			throw new \Phaln\Exceptions\RepositoryException('Pb db dans TagRepository::sauverTagsAdherent(): ' . $e->getMessage());
		}
		
		return $lbOK ;
    }
   
	public function getEntityByName($nomTag, $pbAutoriseCreeNouveau = false) {
        $SQL = 'SELECT * FROM ' . $this->table
                . ' WHERE normalise(nomTag) = :nom' ;
		$reqPrep = $this->db->prepare($SQL);
		$reqPrep->bindValue(':nom', Functions::normalise($nomTag));
// echo $nomTag . "<br>" ;
		$entity = $this->getEntityByReqPrep($reqPrep);
		if (!$entity && $pbAutoriseCreeNouveau) {
			// Il n'y a pas encore d'entité à ce nom, et on autorise la création
			// Créer l'entité maintenant
			$entity = new $this->classMapped(["nomTag"=>$nomTag]) ;
			$entity = $this->sauver($entity) ;
		}
        return $entity;
     }

}
