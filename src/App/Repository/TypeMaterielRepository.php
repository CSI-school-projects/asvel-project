<?php

namespace App\Repository;

use App\Entity\TypeMateriel;
// LG 20200208 déac use Phaln\BDD;

class TypeMaterielRepository extends \Phaln\AbstractRepository
{
/*
    public function __construct() {

        $this->table = 'activite';
        $this->db = BDD::getConnexion();
        if(!$this->db)
            throw new RepositoryException('Pb db dans PersonneRepository::_construct()');

    }
*/
    protected $table = 'typemateriel';                    // le nom de la table manipulée
    protected $classMapped = 'App\Entity\TypeMateriel';    // le nom de la classe mappée
    protected $idFieldName = 'idtypemateriel';            // le nom du champ clé primaire. id par défaut.
    //protected $notFieldProps = [];                    // tableau des propriétés qui ne sont pas des champs de la table sous-jacente
    //protected $uniqueProps = [["nomActivite"]];    // Tableau des combinaisons de champs qui doivent être uniques (ex. [["nomActivite"]], [["nomAdherent", "prenomAdherent", "dateNaissanceAdherent", "mailAdherent"], ["mailAdherent"]])

    public function getEntitesById($id) {
        return $this->getEntityById($id);
    }

    public function createEntite($activite)
    {
        $bindParam = array(
            'nomActivite' => $activite->getNomActivite(),
        );

        // Nouvelle entité
        $query = 'INSERT INTO activite'
            . ' (nomActivite)'
            . ' VALUES (:nomActivite)';

        $reqPrep = $this->db->prepare($query);
        $reqPrep->bindParam(':nomActivite', $bindParam['nomActivite'], \PDO::PARAM_STR);
        $reqPrep->execute();
    }

    public function getAll()
    {
        $query = 'SELECT * FROM '. $this->table.' Order by '.$this->idFieldName.';';
        $reqPrep = $this->db->prepare($query);
        $Result = $this->getAllByReqPrep($reqPrep);
        return $Result;
    }
}
