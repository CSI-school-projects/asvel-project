<?php
namespace App;
use Phaln\BDD;
use \PDO;





class date {

    public function plus_ancienne_date(){
        $infoBdd = BDD::$infoBdd;
        $dsn = "pgsql:host=$infoBdd[host];
            dbname=$infoBdd[dbname]";
        $conBdd = new PDO($dsn, $infoBdd["user"], $infoBdd["pass"]);
        
        $query = "SELECT TO_CHAR(MIN(dateDebutSortie), 'YYYY-MM-DD') AS plusanciennedate FROM Sortie";
        $reqPrep = $conBdd->prepare($query);
        $reqPrep -> execute();
        $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
        $resultat = $row['plusanciennedate'];
        return $resultat;

    }
}
