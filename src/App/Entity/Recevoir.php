<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Entities;

/**
 * Description of Recevoir
 *
 * @author arnau
 */
class Recevoir extends \Phaln\AbstractEntity {

    protected $Id_Adherent = NULL;
    protected $Id_Mail = NULL;

    function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    public function hydrate(array $data = NULL) {
        if (isset($data['idAdherent'])) {
            $this->setId_Adherent($data['idAdherent']);
        }
        if (isset($data['idMail'])) {
            $this->setId_Mail($data['idMail']);
        }
    }

    function getId_Adherent() {
        return $this->Id_Adherent;
    }

    function getId_Mail() {
        return $this->Id_Mail;
    }

    function setId_Adherent($Id_Adherent) {
        $this->Id_Adherent = $Id_Adherent;
    }

    function setId_Mail($Id_Mail) {
        $this->Id_Mail = $Id_Mail;
    }

}
