<?php

namespace App\Entity;

class TypeMiseADispo extends \Phaln\AbstractEntity
{
    protected $idTypeMiseADispo;
    protected $nomTypeMiseADispo;

    public function getNomTypeMiseADispo(){
        return $this->nomTypeMiseADispo;
    }
}
