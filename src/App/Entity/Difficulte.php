<?php

namespace App\Entity;

class Difficulte extends \Phaln\AbstractEntity {
    protected $idDifficulte;
    protected $nomDifficulte;


    public function __construct(array $arr) {
        $this->hydrate($arr);
    }

    function getIdDifficulte() {
        return $this->idDifficulte;
    }

    function getNomDifficulte() {
        return $this->nomDifficulte;
    }

    function setIdDifficulte($idDifficulte) {
        $this->idDifficulte = $idDifficulte;
    }

    function setNomDifficulte($nomDifficulte) {
        $this->nomDifficulte = $nomDifficulte;
    }

}
