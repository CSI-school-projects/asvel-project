<?php

namespace App\Entity;

class Photo extends \Phaln\AbstractEntity {
    protected $idPhoto;
    protected $dateHeurePhoto;
    protected $cheminPhoto;
    protected $idAdherent;
    protected $idSortie;

    public function __construct(array $arr) {
        $this->hydrate($arr);
    }

    function getIdPhoto() {
        return $this->idPhoto;
    }

    function getDateHeurePhoto() {
        return $this->dateHeurePhoto;
    }

    function getCheminPhoto() {
        return $this->cheminPhoto;
    }

    function getIdAdherent() {
        return $this->idAdherent;
    }

    function getIdSortie() {
        return $this->idSortie;
    }

    function setIdPhoto($idPhoto) {
        $this->idPhoto = $idPhoto;
    }

    function setDateHeurePhoto($dateHeurePhoto) {
        $this->dateHeurePhoto = $dateHeurePhoto;
    }
    
    function setCheminPhoto($cheminPhoto) {
        $this->cheminPhoto = $cheminPhoto;
    }

    function setIdAdherent($idAdherent) {
        $this->idAdherent = $idAdherent;
    }

    function setIdSortie($idSortie) {
        $this->idSortie = $idSortie;
    }
}
