<?php
namespace App\Entity;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Autreaffiliation
 *
 * @author Haris
 */
class Autreaffiliation extends \Phaln\AbstractEntity {
    protected $idAutreaffiliation;
    protected $nomAutreaffiliation;
    
    function getIdAutreaffiliation() {
        return $this->idAutreaffiliation;
    }

    function getNomAutreaffiliation() {
        return $this->nomAutreaffiliation;
    }

    function setIdAutreaffiliation($idAutreaffiliation) {
        $this->idAutreaffiliation = $idAutreaffiliation;
    }

    function setNomAutreaffiliation($nomAutreaffiliation) {
        $this->nomAutreaffiliation = $nomAutreaffiliation;
    }


}
