<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Entities;

/**
 * Description of Avoir
 *
 * @author arnau
 */
class Avoir extends \Phaln\AbstractEntity {

    protected $Id_Adherent = NULL;
    protected $Id_Tag = NULL;

    function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    public function hydrate(array $data = NULL) {
        if (isset($data['idAdherent'])) {
            $this->setId_Adherent($data['idAdherent']);
        }
        if (isset($data['idTag'])) {
            $this->setId_Tag($data['idTag']);
        }
    }
    function getId_Adherent() {
        return $this->Id_Adherent;
    }

    function getId_Tag() {
        return $this->Id_Tag;
    }

    function setId_Adherent($Id_Adherent) {
        $this->Id_Adherent = $Id_Adherent;
    }

    function setId_Tag($Id_Tag) {
        $this->Id_Tag = $Id_Tag;
    }



}
