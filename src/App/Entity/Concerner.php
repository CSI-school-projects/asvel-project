<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Entities;

/**
 * Description of Concerner
 *
 * @author arnau
 */
class Concerner extends \Phaln\AbstractEntity {

    protected $Id_Activite = NULL;
    protected $Id_Sortie = NULL;

    function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    public function hydrate(array $data = NULL) {
        if (isset($data['idActivite'])) {
            $this->setId_Activite($data['idActivite']);
        }
        if (isset($data['idSortie'])) {
            $this->setId_Sortie($data['idSortie']);
        }
    }

    function getId_Activite() {
        return $this->Id_Activite;
    }

    function getId_Sortie() {
        return $this->Id_Sortie;
    }

    function setId_Activite($Id_Activite) {
        $this->Id_Activite = $Id_Activite;
    }

    function setId_Sortie($Id_Sortie) {
        $this->Id_Sortie = $Id_Sortie;
    }



}
