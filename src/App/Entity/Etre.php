<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;


/**
 * Description of Etre
 *
 * @author arnau
 */
class Etre extends \Phaln\AbstractEntity {

    protected $Id_Adherent = NULL;
    protected $Id_Role = NULL;

    function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    public function hydrate(array $data = NULL) {
        if (isset($data['idadherent'])) {
            $this->setId_Adherent($data['idadherent']);
        }
        if (isset($data['idrole'])) {
            $this->setId_Role($data['idrole']);
        }
    }

    function getId_Adherent() {
        return $this->Id_Adherent;
    }

    function getId_Role() {
        return $this->Id_Role;
    }

    function setId_Adherent($Id_Adherent) {
        $this->Id_Adherent = $Id_Adherent;
    }

    function setId_Role($Id_Role) {
        $this->Id_Role = $Id_Role;
    }

}
