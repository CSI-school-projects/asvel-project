<?php

namespace App\Entity;

// LG 20200208 début
use App\Repository\RoleRepository;
use App\Repository\PasseportRepository;
use App\Repository\TagsRepository;
use App\Repository\FederationRepository;

// LG 20200208 fin

class Adherent extends \Phaln\AbstractEntity {

    protected $idAdherent;
    protected $nomAdherent;
    protected $prenomAdherent;
    protected $dateNaissanceAdherent;
    protected $licenseAdherent;
    protected $adresseAdherent;
    protected $adresse2Adherent;
    protected $mailAdherent;
    protected $telAdherent;
    protected $cpAdherent;
    protected $villeAdherent;
    protected $paysAdherent;
    protected $nationaliteAdherent;
    protected $sexeAdherent;
    protected $mdpAdherent;
    protected $droitImageAdherent;
    protected $refusMailAdherent;
    protected $commentaireAdherent;
    protected $prenomContact;
    protected $nomContact;
    protected $adresseContact;
    protected $adresse2Contact;
    protected $villeContact;
    protected $cpContact;
    protected $mailContact;
    protected $telContact;
    protected $idTypeCertif;
    protected $certifNomMedecin;
    protected $certifAlpi;
    protected $saeAdherent;
    protected $idautreaffiliation;
    
    // Propriétés utiles pour l'importation
    protected $roles;
    protected $tags;
    protected $federations;
    protected $affiliations;
    protected $passeports;
    protected $numeroLicense;
    protected $nomTypeLicense;
    protected $nomFederationLicense;
    protected $dateLicense;
	
    // Propriétés calculées
    protected $dateFinDerniereAffiliation;

    public function __construct(array $arr = []) {
        $this->hydrate($arr);
    }

// LG 20200208 début
    /*
      function getRoles() {
      return $this->roles;
      }
     */

    /**
     * Renvoyer le tableau des entités rôles de cet adhérent
     * @param boolean $pbIfNotSet : si true, la propriété n'est renvoyée que si elle est déja déinie
     * @return type
     */
    function getRoles($pbIfNotSet = false) {
        if ($this->roles == null && !$pbIfNotSet) {
            // Il faut déterminer les rôles maintenant
            $Repo = new RoleRepository();
            $this->roles = $Repo->getRolesByUser($this->getIdAdherent());
        }
        if (is_string($this->roles)) {
            // On a fourni la liste des rôles sous forme d'une chaine de caractères
            $Repo = new RoleRepository();
            $laRoles = explode(",", $this->roles);
            $this->roles = [];
            foreach ($laRoles as $liRole) {
                if (!$liRole)
                    continue;
                $this->roles[] = $Repo->getEntityById($liRole);
            }
        }
        return $this->roles;
    }

    /**
     * Renvoyer le tableau des entités passeports de cet adhérent
     * @param boolean $pbIfNotSet : si true, la propriété n'est renvoyée que si elle est déja déinie
     * @return type
     */
    function getPasseports($pbIfNotSet = false) {
        if ($this->passeports == null && !$pbIfNotSet) {
            // Il faut déterminer les participations maintenant
            // On a demandé la liste des passeports
            $Repo = new PasseportRepository();
            $this->passeports = $Repo->getPasseportByAdherent($this->getIdAdherent());
        }
        return $this->passeports;
    }

// LG 20200208 fin

    /**
     * Renvoyer le tableau des entités tags de cet adhérent
     * @param boolean $pbIfNotSet : si true, la propriété n'est renvoyée que si elle est déja déinie
     * @return type
     */
    function getTags($pbIfNotSet = false) {
// echo "this->tags = " . $this->tags ;
        if ($this->tags == null && !$pbIfNotSet) {
            // Il faut déterminer les participations maintenant
            // On a demandé la liste des passeports
            $Repo = new TagsRepository();
            $this->tags = $Repo->getTagsByUser($this->getIdAdherent());
        }

        if (is_string($this->tags)) {
            // On a fourni la liste des tags sous forme d'une chaine de caractères
            $Repo = new TagsRepository();
            $laTags = explode(",", $this->tags);
            $this->tags = [];
            foreach ($laTags as $liTag) {
                if (!$liTag)
                    continue;
                if (is_numeric($liTag)) {
                    $this->tags[] = $Repo->getEntityById($liTag);
                } else if (is_string($liTag)) {
                    $this->tags[] = $Repo->getEntityByName($liTag, true);
                } else {
                    throw new \Exception("Le Tag fourni n'est ni numérique ni string");
                }
            }
        }
        return $this->tags;
    }

    // Renvoyer la liste des fédérations
    function getFederations($pbIfNotSet = false) {
        if ($this->federations == null && !$pbIfNotSet) {
            // Il faut déterminer les participations maintenant
            // On a demandé la liste des fédérations
            $Repo = new FederationRepository();
            $this->federations = $Repo->getDistinctByAdherent($this->getIdAdherent());
        }
        return $this->federations;
    }

    // Renvoyer la liste des fédérations
    function getAffiliations($pbIfNotSet = false) {
        if ($this->federations == null && !$pbIfNotSet) {
            // Il faut déterminer les participations maintenant
            // On a demandé la liste des fédérations
            $repo = new \App\Repository\AffilierRepository();
            $this->federations = $repo->getAllByAdherent($this->getIdAdherent());
        }
        return $this->federations;
    }
// LG 20201011
    function getDernièreAffiliation() {
	$repo = new \App\Repository\AffilierRepository();
        return $repo->getDernièreAffiliationAdherent($this->getIdAdherent());
    }
	
// LG 20201011
    function getDateDernièreAffiliation($pbIfNotSet = false) {
		if ($this->dateFinDerniereAffiliation == null && !$pbIfNotSet) {
			// $this->dateFinDerniereAffiliation est rempli par défaut par AdherentRepository->getAll(true)
			// Mais si ca n'a pas été fait, on peut le récupérer manuellement ici
			$repo = new \App\Repository\AffilierRepository();
			$dernièreAffiliationAdherent = $repo->getDernièreAffiliationAdherent($this->getIdAdherent());
			if ($dernièreAffiliationAdherent) {
				$this->dateFinDerniereAffiliation = $dernièreAffiliationAdherent->getDateFinAffiliation() ;
			} else {
				return null ;
			}
		}
		return $this->dateFinDerniereAffiliation ;
    }
    
    // LG 20210117
    // Renvoyer un objet vers la dernière sortie faite par l'adhérent
    function getDernièreSortie($piNbJoursEnArrière) {
        $repo = new \App\Repository\ParticiperRepository();
        $derniereSortie = $repo->getDernièreSortieAdhérent($this->getIdAdherent(), $piNbJoursEnArrière);
        return $derniereSortie ;
    }
    // LG 20210117
    // Renvoyer le nombre de sorties faites par l'adhérent
    function getNbDernièresSorties($piNbJoursEnArrière) {
        $repo = new \App\Repository\ParticiperRepository();
        $nb = $repo->getNbDernièresSortiesAdhérent($this->getIdAdherent(), $piNbJoursEnArrière);
        return $nb ;
    }
    
    // Renvoyer une chaine d'informations sur les dernières sorties refusées
    // LG 20210117
    function getInfosSortiesRefusées($piNbJoursEnArrière) {
        $repo = new \App\Repository\ParticiperRepository();
        $derniereSortie = $repo->getInfosSortiesRefusées($this->getIdAdherent(), $piNbJoursEnArrière);
        return $derniereSortie ;
    }
    
    // Renvoyer  un objet vers la prochaine sortie demandée par l'adhérent
    // LG 20210117
    function getProchaineDemandeDeParticipation() {
        $repo = new \App\Repository\ParticiperRepository();
        $derniereSortie = $repo->getProchaineDemandeDeParticipation($this->getIdAdherent());
        return $derniereSortie ;
    }

// LG 20201010 old    function getNomUsuel(){
    function getNomUsuel($pbInclDateFinAdhésion = false){
        // Sera à paramétrer
// LG 20201010 old        return $this->prenomAdherent . " " . $this->nomAdherent ;
        $nom = $this->prenomAdherent . " " . $this->nomAdherent ;
		if ($pbInclDateFinAdhésion) {
			// il faut rechercher la date de fin de la license actuelle
			
//			$dernièreAffiliation = $this->getDernièreAffiliation() ;
//			if ($dernièreAffiliation) {
//				$nom .= " (-> " . substr($dernièreAffiliation->getDateFinAffiliation(), 6, 4) . ")" ;
//			} else {
//				$nom .= " (non inscrit)" ;
//			}

			if ($dateFinDerniereAffiliation = $this->getDateDernièreAffiliation()) {
				$nom .= " (-> " . substr($dateFinDerniereAffiliation, 6, 4) . ")" ;
			} else {
				$nom .= " (non inscrit)" ;
			}
		}
		
		return $nom ;
    }
	
    function setFederation() {
        $this->federation = $federation;
    }

    function setTags($tags) {
        $this->tags = $tags;
    }

    function getIdAdherent() {
        return $this->idAdherent;
    }

    function getNomAdherent() {
        return $this->nomAdherent;
    }

    function getPrenomAdherent() {
        return $this->prenomAdherent;
    }

    function getDateNaissanceAdherent() {
        return $this->dateNaissanceAdherent;
    }

    function getLicenseAdherent() {
        return $this->licenseAdherent;
    }

    function getAdresseAdherent() {
        return $this->adresseAdherent;
    }

    function getAdresse2Adherent() {
        return $this->adresse2Adherent;
    }

    function getMailAdherent() {
        return $this->mailAdherent;
    }

    function getTelAdherent() {
        return $this->telAdherent;
    }

    function getCpAdherent() {
        return $this->cpAdherent;
    }

    function getVilleAdherent() {
        return $this->villeAdherent;
    }

    function getPaysAdherent() {
        return $this->paysAdherent;
    }

    function getNationaliteAdherent() {
        return $this->nationaliteAdherent;
    }

    function getSexeAdherent() {
        return $this->sexeAdherent;
    }

    function getMdpAdherent() {
        return $this->mdpAdherent;
    }

    function getDroitImageAdherent() {
        return $this->droitImageAdherent;
    }

    function getRefusMailAdherent() {
        return $this->refusMailAdherent;
    }

    function getIdautreaffiliation() {
        return $this->idautreaffiliation;
    }

    function getIdTypeReglementAdherent() {
        return $this->idTypeReglementAdherent;
    }

    function getMontantReglementAdherent() {
        return $this->montantReglementAdherent;
    }

    function getCommentaireAdherent() {
        return $this->commentaireAdherent;
    }

    function getPrenomContact() {
        return $this->prenomContact;
    }

    function getNomContact() {
        return $this->nomContact;
    }

    function getAdresseContact() {
        return $this->adresseContact;
    }

    function getAdresse2Contact() {
        return $this->adresse2Contact;
    }

    function getVilleContact() {
        return $this->villeContact;
    }

    function getCpContact() {
        return $this->cpContact;
    }

    function getMailContact() {
        return $this->mailContact;
    }

    function getTelContact() {
        return $this->telContact;
    }

    function getIdTypeCertif() {
        return $this->idTypeCertif;
    }

    function getCertifNomMedecin() {
        return $this->certifNomMedecin;
    }

    function getCertifAlpi() {
        return $this->certifAlpi;
    }

    function getSaeAdherent() {
        return $this->saeAdherent;
    }

    function setIdAdherent($idAdherent) {
        $this->idAdherent = $idAdherent;
    }

    function setNomAdherent($nomAdherent) {
        $this->nomAdherent = $nomAdherent;
    }

    function setPrenomAdherent($prenomAdherent) {
        $this->prenomAdherent = $prenomAdherent;
    }

    function setDateNaissanceAdherent($dateNaissanceAdherent) {
        $this->dateNaissanceAdherent = $dateNaissanceAdherent;
    }

    function setLicenseAdherent($licenseAdherent) {
        $this->licenseAdherent = $licenseAdherent;
    }

    function setIdautreaffiliation($idautreaffiliation) {
        $this->idautreaffiliation = $idautreaffiliation;
    }

    function setAdresseAdherent($adresseAdherent) {
        $this->adresseAdherent = $adresseAdherent;
    }

    function setAdresse2Adherent($adresse2Adherent) {
        $this->adresse2Adherent = $adresse2Adherent;
    }

    function setMailAdherent($mailAdherent) {
        $this->mailAdherent = $mailAdherent;
    }

    function setTelAdherent($telAdherent) {
        $this->telAdherent = $telAdherent;
    }

    function setCpAdherent($cpAdherent) {
        $this->cpAdherent = $cpAdherent;
    }

    function setVilleAdherent($villeAdherent) {
        $this->villeAdherent = $villeAdherent;
    }

    function setPaysAdherent($paysAdherent) {
        $this->paysAdherent = $paysAdherent;
    }

    function setNationaliteAdherent($nationaliteAdherent) {
        $this->nationaliteAdherent = $nationaliteAdherent;
    }

    function setSexeAdherent($sexeAdherent) {
        $this->sexeAdherent = $sexeAdherent;
    }

    function setMdpAdherent($mdpAdherent) {
        $this->mdpAdherent = $mdpAdherent;
    }

    function setDroitImageAdherent($droitImageAdherent) {
        $this->droitImageAdherent = $droitImageAdherent;
    }

    function setRefusMailAdherent($refusMailAdherent) {
        $this->refusMailAdherent = $refusMailAdherent;
    }

    function setIdTypeReglementAdherent($idTypeReglementAdherent) {
        $this->idTypeReglementAdherent = $idTypeReglementAdherent;
    }

    function setMontantReglementAdherent($montantReglementAdherent) {
        $this->montantReglementAdherent = $montantReglementAdherent;
    }

    function setCommentaireAdherent($commentaireAdherent) {
        $this->commentaireAdherent = $commentaireAdherent;
    }

    function setPrenomContact($prenomContact) {
        $this->prenomContact = $prenomContact;
    }

    function setNomContact($nomContact) {
        $this->nomContact = $nomContact;
    }

    function setAdresseContact($adresseContact) {
        $this->adresseContact = $adresseContact;
    }

    function setAdresse2Contact($adresse2Contact) {
        $this->adresse2Contact = $adresse2Contact;
    }

    function setVilleContact($villeContact) {
        $this->villeContact = $villeContact;
    }

    function setCpContact($cpContact) {
        $this->cpContact = $cpContact;
    }

    function setMailContact($mailContact) {
        $this->mailContact = $mailContact;
    }

    function setTelContact($telContact) {
        $this->telContact = $telContact;
    }

    function setIdTypeCertif($idTypeCertif) {
        $this->idTypeCertif = $idTypeCertif;
    }

    function setCertifNomMedecin($certifNomMedecin) {
        $this->certifNomMedecin = $certifNomMedecin;
    }

    function setCertifAlpi($certifAlpi) {
        $this->certifAlpi = $certifAlpi;
    }

    function setSaeAdherent($saeAdherent) {
        $this->saeAdherent = $saeAdherent;
    }

    function setRoles($roles) {
        $this->roles = $roles;
    }
}
