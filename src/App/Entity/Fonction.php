<?php

namespace App\Entity;

class Fonction extends \Phaln\AbstractEntity {
    protected $idFonction;
    protected $nomFonction;
    
            
    // Renvoyer true si la fonction est une fonction d'encadrement
    // LG 20210117
    public function estEncadrant() {
        return $this->getNomFonction() == "Encadrant" || $this ->getNomFonction() == "Co-encadrant" ;
    }

}
