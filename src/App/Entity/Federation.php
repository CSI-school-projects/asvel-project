<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

/**
 * Description of Federation
 *
 * @author Haris
 */
class Federation extends \Phaln\AbstractEntity {

    protected $idFederation = NULL;
    protected $nomFederation = '';
    protected $nbJoursValiditeAffiliation ;        // -1 pour "1 an à partir du 1er septembre"; -2 pour "1 an à partir du 1er novembre", sinon: nombre de jours
    
    function getDateFinAffiliation($psDateDebutAffiliation) {
        $lsDateFinAffiliation = null ;
        // S'assurer de bien avoir une date
        $ldDebutAffiliation = strtotime($psDateDebutAffiliation) ;
        if ($this->nbJoursValiditeAffiliation == -1) {
            // "1 an à partir du 1er septembre"
            $mois = date("n", $ldDebutAffiliation) ;
            $année = date("Y", $ldDebutAffiliation) ;
            if ($mois >= 9) {
                // Inscription à partir de septembre : fin l'année suivante
                $année++ ;
            }
            $lsDateFinAffiliation = date("m/d/Y", mktime(0, 0, 0, 8, 31, $année)) ;
        } else if($this->nbJoursValiditeAffiliation == -2) {
            // "1 an à partir du 1er novembre"
            $mois = date("n", $ldDebutAffiliation) ;
            $année = date("Y", $ldDebutAffiliation) ;
            if ($mois >= 11) {
                // Inscription à partir de septembre : fin l'année suivante
                $année++ ;
            }
            $lsDateFinAffiliation = date("m/d/Y", mktime(0, 0, 0, 10, 31, $année)) ;
        } else {
            // Validité un certain nb de jours
            $ldFinAffiliation = new \DateTime($psDateDebutAffiliation) ;
            $ldFinAffiliation->add(new \DateInterval('P' . ($this->nbJoursValiditeAffiliation - 1) . 'D')) ;
            $lsDateFinAffiliation = $ldFinAffiliation->format("m/d/Y") ;
        }
        return $lsDateFinAffiliation ;

    }

}
