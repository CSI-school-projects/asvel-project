<?php

namespace App\Entity;

use App\Repository\AdherentRepository;
use App\Repository\TypeMaterielRepository;
use App\Repository\TypeMiseADispoRepository;

class Materiel extends \Phaln\AbstractEntity
{
    protected $idMateriel;
    protected $tailleMateriel;
    protected $pointureMateriel;
    protected $modeleMateriel;
    protected $latitude1Materiel;
    protected $latitude2Materiel;
    protected $longitude1Materiel;
    protected $longitude2Materiel;
    protected $dateAchatMateriel;
    protected $commentaireMateriel;
    protected $paysCarteMateriel;
    protected $idAdherent;
    protected $idTypeMateriel;
    protected $idTypeMiseADispo;

/*
    public function getIdAdherent()
    {
        $repo = new AdherentRepository() ;
        return $repo->getEntitesById($this->getIdAdherent()) ;
    }

    public function getIdTypeMateriel(){
        $repo = new TypeMaterielRepository();
        return $repo->getEntitesById($this->getIdTypeMateriel());
    }

    public function getIdTypeMiseADispo()
    {
        $repo = new TypeMiseADispoRepository();
        return $repo->getEntityById($this->getIdTypeMiseADispo());
    }*/
}
