<?php
namespace App;
use Phaln\BDD;
use \PDO;
require_once '../../config/globalConfig.php';



class Statistique {

    public function nbAdherents(string $nomActivite, string $dateDebut, string $dateFin){

        $infoBdd = BDD::$infoBdd;
        $dsn = "pgsql:host=$infoBdd[host];
            dbname=$infoBdd[dbname]";
        $conBdd = new PDO($dsn, $infoBdd["user"], $infoBdd["pass"]);
    
        $query = "SELECT COUNT(*) AS adherents FROM (SELECT DISTINCT AD.* FROM Adherent AD "
            ."JOIN Participer P ON P.idAdherent = AD.idAdherent "
            ."JOIN Sortie S ON S.idSortie = P.idSortie";

        If ($nomActivite=="toutes"){
            $query .= " WHERE ";
        } Else {
            $query .= " JOIN Concerner C ON C.idSortie = S.idSortie "
                ."JOIN Activite A ON A.idActivite = C.idActivite "
                ."WHERE A.nomActivite = '$nomActivite' AND ";
        }

        $query .= "TO_DATE('$dateDebut', 'YYYY-MM-DD') <= dateDebutSortie "
            ."AND TO_DATE('$dateFin', 'YYYY-MM-DD') >= dateFinSortie) AS participants";

        $reqPrep = $conBdd->prepare($query);
        $reqPrep -> execute();
        $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
        $resultat = $row['adherents'];
        return $resultat;

    }


    public function nbSorties(string $nomActivite, string $dateDebut, string $dateFin){
        
        $infoBdd = BDD::$infoBdd;
        $dsn = "pgsql:host=$infoBdd[host];
            dbname=$infoBdd[dbname]";
        $conBdd = new PDO($dsn, $infoBdd["user"], $infoBdd["pass"]);

        $query = "SELECT COUNT(S.*) AS sorties FROM Sortie S ";

        If ($nomActivite=="toutes"){
            $query .= " WHERE ";
        } Else {
            $query .= " JOIN Concerner C ON C.idSortie = S.idSortie "
                ." JOIN Activite A ON A.idActivite = C.idActivite "
                ."WHERE A.nomActivite = '$nomActivite' AND ";
        }

        $query .= "TO_DATE('$dateDebut', 'YYYY-MM-DD') <= dateDebutSortie "
            ."AND TO_DATE('$dateFin', 'YYYY-MM-DD') >= dateFinSortie";

        $reqPrep = $conBdd->prepare($query);
        $reqPrep -> execute();
        $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
        $resultat = $row['sorties'];
        return $resultat;

    }


    public function nbAdherentsMoyen(string $nomActivite, string $dateDebut, string $dateFin){
        $infoBdd = BDD::$infoBdd;
        $dsn = "pgsql:host=$infoBdd[host];
            dbname=$infoBdd[dbname]";
        $conBdd = new PDO($dsn, $infoBdd["user"], $infoBdd["pass"]);

        $query = "SELECT ROUND(CAST(COUNT(AD.*) AS numeric)/(SELECT COUNT(S.*) FROM Sortie S";

        If ($nomActivite=="toutes"){
            $query .= " WHERE TO_DATE('$dateDebut', 'YYYY-MM-DD') <= dateDebutSortie "
                ."AND TO_DATE('$dateFin', 'YYYY-MM-DD') >= dateDebutSortie)) "
                ."AS moyenne FROM Adherent AD "
                ."JOIN Participer P ON P.idAdherent = AD.idAdherent "
                ."JOIN Sortie S ON S.idSortie = P.idSortie "
                ."WHERE ";
        } Else {
            $query .= " JOIN Concerner C ON C.idSortie = S.idSortie "
                ."JOIN Activite A ON A.idActivite = C.idActivite "
                ."WHERE A.nomActivite = '$nomActivite' "
                ."AND TO_DATE('$dateDebut', 'YYYY-MM-DD') <= dateDebutSortie "
                ."AND TO_DATE('$dateFin', 'YYYY-MM-DD') >= dateDebutSortie)) "
                ."AS moyenne FROM Adherent AD "
                ."JOIN Participer P ON P.idAdherent = AD.idAdherent "
                ."JOIN Sortie S ON S.idSortie = P.idSortie "
                ."JOIN Concerner C ON C.idSortie = S.idSortie "
                ."JOIN Activite A ON A.idActivite = C.idActivite "
                ."WHERE A.nomactivite = '$nomActivite' AND ";
        }

        $query .= "TO_DATE('$dateDebut', 'YYYY-MM-DD') <= dateDebutSortie "
            ."AND TO_DATE('$dateFin', 'YYYY-MM-DD') >= dateFinSortie";
        

        $reqPrep = $conBdd->prepare($query);
        $reqPrep -> execute();
        $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
        $resultat = $row['moyenne'];
        return $resultat;

    }


    public function beneficeSortie(string $nomActivite, string $dateDebut, string $dateFin){
        $fraisKilometrique = FRAIS_KILOMETRIQUES;
        $infoBdd = BDD::$infoBdd;
        $dsn = "pgsql:host=$infoBdd[host];
            dbname=$infoBdd[dbname]";
        $conBdd = new PDO($dsn, $infoBdd["user"], $infoBdd["pass"]);

        $query = "SELECT SUM(benefice) AS total, AVG(benefice) AS moyenne "
                    ."FROM (SELECT S.idsortie, "
                                ."CASE WHEN COALESCE(S.tarifsortie, 0) = 0 "
                                    ."THEN SUM(P.montanttotal) "
                                    ."ELSE COALESCE(S.tarifsortie, 0) * (COUNT(*)::double precision - 1) END "
                                    ."- (COALESCE(S.coutautoroute, 0) * COALESCE(S.nbVoituresSortie, 0) "
                                    ."+ $fraisKilometrique * COALESCE(S.distanceSortie, 0) * COALESCE(S.nbVoituresSortie, 0) "
                                    ."+ COALESCE(s.fraisannexe, 0))/ COUNT(*)::double precision AS benefice "
                                ."FROM participer p JOIN sortie s ON p.idsortie = s.idsortie";

        If ($nomActivite=="toutes"){
            $query = $query." WHERE ";
        } Else {
            $query = $query." JOIN Concerner C ON C.idSortie = S.idSortie "
            ."JOIN Activite A ON A.idActivite = C.idActivite "
            ."WHERE A.nomactivite = '$nomActivite' AND";
        }

        $query = $query." TO_DATE('2020-05-16', 'YYYY-MM-DD') <= dateDebutSortie " 
                        ."AND TO_DATE('2023-01-05', 'YYYY-MM-DD') >= dateFinSortie "
                        ."GROUP BY s.idsortie "
                        .") AS beneficesparsortie";

        $reqPrep = $conBdd->prepare($query);
        $reqPrep -> execute();
        $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
        $resultat = $row;
        return $resultat;

    }


    public function nbParticipations(string $nomActivite, string $dateDebut, string $dateFin){

        $infoBdd = BDD::$infoBdd;
        $dsn = "pgsql:host=$infoBdd[host];
            dbname=$infoBdd[dbname]";
        $conBdd = new PDO($dsn, $infoBdd["user"], $infoBdd["pass"]);
    
        $query = "SELECT ROUND(CAST(COUNT(*) AS Numeric)/(Select COUNT(*) FROM Adherent)) AS Moyenne "
            ."FROM Participer P  "
            ."JOIN Sortie S ON S.idSortie = P.idSortie";

        If ($nomActivite=="toutes"){
            $query .= " WHERE ";
        } Else {
            $query .= " JOIN Concerner C ON C.idSortie = S.idSortie "
                ."JOIN Activite A ON A.idActivite = C.idActivite "
                ."WHERE A.nomActivite = '$nomActivite' AND ";
        }

        $query .= "TO_DATE('$dateDebut', 'YYYY-MM-DD') <= dateDebutSortie "
            ."AND TO_DATE('$dateFin', 'YYYY-MM-DD') >= dateFinSortie";

        $reqPrep = $conBdd->prepare($query);
        $reqPrep -> execute();
        $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
        $resultat = $row['moyenne'];
        return $resultat;

    }


    public function nbSortiesAnnulees(string $nomActivite, string $dateDebut, string $dateFin){

        $infoBdd = BDD::$infoBdd;
        $dsn = "pgsql:host=$infoBdd[host];
            dbname=$infoBdd[dbname]";
        $conBdd = new PDO($dsn, $infoBdd["user"], $infoBdd["pass"]);

        $query = "SELECT COUNT(S.*) AS annulations FROM Sortie S ";

        If ($nomActivite=="toutes"){
            $query .= " WHERE ";
        } Else {
            $query .= " JOIN Concerner C ON C.idSortie = S.idSortie "
                ." JOIN Activite A ON A.idActivite = C.idActivite "
                ."WHERE A.nomActivite = '$nomActivite' AND ";
        }

        $query .= "TO_DATE('$dateDebut', 'YYYY-MM-DD') <= dateDebutSortie "
            ."AND TO_DATE('$dateFin', 'YYYY-MM-DD') >= dateFinSortie "
            ."AND idRaisonAnnulation NOTNULL";

        $reqPrep = $conBdd->prepare($query);
        $reqPrep -> execute();
        $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
        $resultat = $row['annulations'];
        return $resultat;

    }


    public function nbDesistements(string $nomActivite, string $dateDebut, string $dateFin){

        $infoBdd = BDD::$infoBdd;
        $dsn = "pgsql:host=$infoBdd[host];
            dbname=$infoBdd[dbname]";
        $conBdd = new PDO($dsn, $infoBdd["user"], $infoBdd["pass"]);
    
        $query = "SELECT COUNT(*) AS desistements "
            ."FROM Participer P "
            ."JOIN Sortie S ON S.idSortie = P.idSortie";

        If ($nomActivite=="toutes"){
            $query .= " WHERE ";
        } Else {
            $query .= " JOIN Concerner C ON C.idSortie = S.idSortie "
                ."JOIN Activite A ON A.idActivite = C.idActivite "
                ."WHERE A.nomActivite = '$nomActivite' AND ";
        }

        $query .= "TO_DATE('$dateDebut', 'YYYY-MM-DD') <= dateDebutSortie "
            ."AND TO_DATE('$dateFin', 'YYYY-MM-DD') >= dateFinSortie "
            ."AND P.idRaisonAnnulation NOTNULL";

        $reqPrep = $conBdd->prepare($query);
        $reqPrep -> execute();
        $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
        $resultat = $row['desistements'];
        return $resultat;
    
    }

    public function nbSortiesParAdherentTous(string $nomActivite, string $dateDebut, string $dateFin){

        $infoBdd = BDD::$infoBdd;
        $dsn = "pgsql:host=$infoBdd[host];
            dbname=$infoBdd[dbname]";
        $conBdd = new PDO($dsn, $infoBdd["user"], $infoBdd["pass"]);
    
        $query = "SELECT CAST(nombre_de_sortie.nombre_sorties AS Numeric)"
                                    ."/(SELECT COUNT(*) FROM adherent) as nombre_moyen_sorties "
                    ."FROM (SELECT COUNT(*) as nombre_sorties "
                    ."FROM Sortie S ";

        If ($nomActivite=="toutes"){
            $query .= " WHERE ";
        } Else {
            $query = $query."  JOIN Concerner C ON C.idSortie = S.idSortie "
                ." JOIN Activite A ON A.idActivite = C.idActivite "
                ."WHERE A.nomActivite = '$nomActivite' AND ";
        }
        
        $query .= "TO_DATE('$dateDebut', 'YYYY-MM-DD') <= dateDebutSortie "
            ."AND TO_DATE('$dateFin', 'YYYY-MM-DD') >= dateFinSortie) AS nombre_de_sortie";

        $reqPrep = $conBdd->prepare($query);
        $reqPrep -> execute();
        $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
        $resultat = $row['nombre_moyen_sorties'];
        return $resultat;

    }


    public function nbSortiesParAdherentRestreint(string $nomActivite, string $dateDebut, string $dateFin){

        $infoBdd = BDD::$infoBdd;
        $dsn = "pgsql:host=$infoBdd[host];
            dbname=$infoBdd[dbname]";
        $conBdd = new PDO($dsn, $infoBdd["user"], $infoBdd["pass"]);
    
        $query = "SELECT ROUND(AVG(sorties_par_adherent.nombre_sorties)) as nombre_moyen_sorties "
            ."FROM (SELECT idAdherent, COUNT(P.idSortie) as nombre_sorties "
            ."FROM Participer P "
            ."JOIN Sortie S ON P.idSortie = S.idSortie";

        If ($nomActivite=="toutes"){
            $query .= " WHERE ";
        } Else {
            $query = $query." JOIN Concerner C ON C.idSortie = S.idSortie "
                ."JOIN Activite A ON A.idActivite = C.idActivite "
                ."WHERE A.nomActivite = '$nomActivite' AND ";
        }
        
        $query .= "TO_DATE('$dateDebut', 'YYYY-MM-DD') <= dateDebutSortie "
            ."AND TO_DATE('$dateFin', 'YYYY-MM-DD') >= dateFinSortie "
            ."GROUP BY idAdherent) AS sorties_par_adherent";

        $reqPrep = $conBdd->prepare($query);
        $reqPrep -> execute();
        $row = $reqPrep->fetch(\PDO::FETCH_ASSOC);
        $resultat = $row['nombre_moyen_sorties'];
        return $resultat;

    }

}
