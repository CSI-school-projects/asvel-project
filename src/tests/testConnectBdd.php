<?php

// !!!! composer require facebook/graph-sdk 

declare(strict_types=1);
require_once '../../config/globalConfig.php';
require_once '../../config/appConfig.php';

//require_once'../../src/App/Repository/SortieRepository.php';

use App\Repository\SortieRepository;
use App\Repository\ActiviteRepository;
use App\Entity\Activite;
use App\Security;
use Phaln\BDD;

$loRepo = new SortieRepository();
$ActiviteRep = new ActiviteRepository();


// Récupérer les paramètres depuis le tableaux $infoBdd de appConfig.php
$hostname = $infoBdd['host'];
$dbname = $infoBdd['dbname'];
$charset = $infoBdd['charset'];
$username = $infoBdd['user'];
$password = $infoBdd['pass'];

// Construction du dsn 
$dsn = "pgsql:host=$hostname;
                dbname=$dbname;
                options='--client_encoding=$charset'";

//On établit la connexion
try
{
    // Création de l'objet PDO
    $pdo = new PDO($dsn, $username, $password);
    // Préparation de la requête SQL
    $stmt = $pdo->prepare('SELECT * FROM activite'); 
    // Execution de la requête préparé
    $stmt->execute();
    // Affciher tout le résultat de la requête
    $resultats = $stmt->fetchAll();
    dump_var($resultats, TRUE, "Selectionner toutes les activites :");

}catch(PDOException $e)
{
    // En cas d'erreur on affiche le message d'erreur
    echo "Erreur".$e->getMessage();
}
?>